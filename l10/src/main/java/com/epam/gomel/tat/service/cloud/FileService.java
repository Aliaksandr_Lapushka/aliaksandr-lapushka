package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.utils.CloudData;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileService {

    private static final int NAME_SIZE = 5;
    private static final int STRING_SIZE = 10;

    public static String createFile() throws InterruptedException {
        String fileName = RandomStringUtils.randomAlphanumeric(NAME_SIZE) + ".txt";
        File file = new File(CloudData.PATH_FOR_UPLOADING, fileName);
        try (FileWriter writer = new FileWriter(file, false)) {
            writer.write(RandomStringUtils.randomAlphabetic(STRING_SIZE));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        Thread.sleep(CloudData.DEF_THREAD_SLEEP);
        return fileName;
    }

    public static void deleteFile(String file) {
        try {
            Files.delete(Paths.get(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
