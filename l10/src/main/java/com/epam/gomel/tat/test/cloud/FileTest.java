package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.framework.utils.CloudData;
import com.epam.gomel.tat.page.cloud.FolderPage;
import com.epam.gomel.tat.service.cloud.FileService;
import com.epam.gomel.tat.service.cloud.FolderNameService;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FileTest extends CloudBaseTest {

    protected String fileName;
    protected String folderName;

    @BeforeClass(description = "Create file")
    public void createFile() throws InterruptedException {
        fileName = FileService.createFile();

    }

    @BeforeClass(description = "Create folder name")
    public void createFolderName() {
        folderName = FolderNameService.createFolderName();
    }

    @Test(description = "Upload Test", priority = 0)
    public void checkUploadFile() {
        cloudLoggedinPage.uploadFile(CloudData.PATH_FOR_UPLOADING + fileName);
        Assert.assertTrue(cloudLoggedinPage.isElementPresent(fileName));
    }

    @Test(description = "File drag'n'drop test", priority = 1, dependsOnMethods = "checkUploadFile")
    public void checkDragDropFile() {
        cloudLoggedinPage.createRemoteFolder(folderName);
        WebElement draggable = cloudLoggedinPage.getExactElement(fileName);
        WebElement droppable = cloudLoggedinPage.getExactElement(folderName);
        cloudLoggedinPage.dragNdrop(draggable, droppable);
        //FolderPage folderPage = new cloudLoggedinPage.lightClick(driver, droppable);
        FolderPage folderPage = cloudLoggedinPage.openFolder(folderName);
        //new WebDriverWait(driver, 1);
        Assert.assertTrue(folderPage.isElementPresent(fileName));
    }

    @AfterClass(description = "Deleting file")
    public void deleteFile() {
        FileService.deleteFile(CloudData.PATH_FOR_UPLOADING + fileName);
    }
}
