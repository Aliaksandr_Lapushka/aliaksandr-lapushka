package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.service.cloud.FolderNameService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FolderTest extends CloudBaseTest {

    protected String folderName;

    @BeforeClass(description = "Create folder name")
    public void createFolderName() {
        folderName = FolderNameService.createFolderName();
    }

    @Test(description = "Create folder test", priority = 0)
    public void checkCreateFolder() {
        cloudLoggedinPage.createRemoteFolder(folderName);
        Assert.assertTrue(cloudLoggedinPage.isElementPresent(folderName));
    }

    @Test(description = "Delete new folder test", priority = 1)
    public void checkDeleteFolder() throws Exception {
        cloudLoggedinPage.checkExactElement(folderName);
        cloudLoggedinPage.delete();
        Assert.assertTrue(cloudLoggedinPage.isDeleted(folderName));
    }
}
