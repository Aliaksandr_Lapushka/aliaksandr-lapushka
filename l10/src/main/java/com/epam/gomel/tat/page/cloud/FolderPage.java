package com.epam.gomel.tat.page.cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FolderPage extends CloudLoggedinPage {

    protected static final String FILE_LOCATOR_TEMPLATE = "//span[@class='b-filename__name'][contains(text(), %s)]";

    public FolderPage(WebDriver driver) {
        super(driver);
    }

    public boolean isElementPresent(String fileName) {
        String resultingLocator = String.format(FILE_LOCATOR_TEMPLATE, fileName);
        System.out.println(resultingLocator);
        By by = By.xpath(resultingLocator);
        waitForReadiness(by);
        return !driver.findElements(by).isEmpty();
    }

}
