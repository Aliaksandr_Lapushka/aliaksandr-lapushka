package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.service.cloud.FolderNameService;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import org.testng.annotations.Test;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class ShareTest extends CloudBaseTest {

    public WebElement element;

    protected String elementName;
    protected String windowTitle;

    @Test(description = "Share cloud content test")
    public void contentShareCheck() throws IOException, UnsupportedFlavorException {
        if (cloudLoggedinPage.isEmptyCloud()) {
            elementName = FolderNameService.createFolderName();
            cloudLoggedinPage.createRemoteFolder(elementName);
            element = cloudLoggedinPage.getExactElement(elementName);
        } else {
            elementName = cloudLoggedinPage.randomElement();
            element = cloudLoggedinPage.getExactElement(elementName);
        }
        String link = cloudLoggedinPage.getPublicLink(element);
        driver.get(link);
        windowTitle = driver.getTitle();
        Assert.assertTrue(windowTitle.matches(elementName + ".*"));
    }
}
