package com.epam.gomel.tat.page.cloud;

import com.epam.gomel.tat.framework.utils.CloudData;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;

import javax.swing.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by Aliaksandr Lapushka on 28.07.2017.
 */
public class PageObject {

    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void waitForVisibility(WebElement element) {
        new WebDriverWait(driver, CloudData.DEF_EXPLICIT_TIMEOUT)
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }

    void waitForLoad(WebDriver driver) {
        new WebDriverWait(driver, CloudData.DEF_EXPLICIT_TIMEOUT).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    protected void waitForClickability(WebElement element) {
        new WebDriverWait(driver, CloudData.DEF_EXPLICIT_TIMEOUT)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForReadiness(By locator) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(CloudData.DEF_EXPLICIT_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(CloudData.DEF_POLLING_INTRVAL, TimeUnit.MILLISECONDS)
                .ignoring(org.openqa.selenium.NoSuchElementException.class);
        WebElement webElement = wait.until(
                new Function<WebDriver, WebElement>() {
                    public WebElement apply(WebDriver driver) {
                        return driver.findElement(locator);
                    }
                }
        );
        //return webElement;
    }
    //}

    public void lightClick(WebDriver driver, WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='4px groove green'", element);
        waitForVisibility(element);
        //Thread.sleep(CloudData.DEF_THREAD_SLEEP);
        js.executeScript("arguments[0].style.border=''", element);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.build().perform();
        element.click();
    }
}
