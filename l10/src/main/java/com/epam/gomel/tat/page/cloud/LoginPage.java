package com.epam.gomel.tat.page.cloud;

import com.epam.gomel.tat.framework.utils.CloudData;
//import com.epam.gomel.tat.framework.utils.HighlightElem;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends PageObject {

    public static final String LOGIN_PROMPT_LOCATOR = ".nav-inner-col-try__bt";
    public static final String LOGIN_INPUT_LOCATOR = "input#ph_login";
    public static final String PASSWORD_INPUT_LOCATOR = "input#ph_password";
    public static final String SUBMIT_BUTTON_LOCATOR = "input.x-ph__button__input";
    public static final String SIGNIN_POPUP_LOCATOR = "form#x-ph__authForm__popup";

    @FindBy(how = How.CSS, using = LOGIN_PROMPT_LOCATOR)
    private WebElement loginPrompt;

    @FindBy(how = How.CSS, using = LOGIN_INPUT_LOCATOR)
    private WebElement loginInput;

    @FindBy(how = How.CSS, using = PASSWORD_INPUT_LOCATOR)
    private WebElement passInput;

    @FindBy(how = How.CSS, using = SUBMIT_BUTTON_LOCATOR)
    private WebElement submitButton;

    @FindBy(how = How.CSS, using = SIGNIN_POPUP_LOCATOR)
    private WebElement signinPopup;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(CloudData.CLOUD_START_PAGE);
    }

    public CloudLoggedinPage login(String userLogin, String userPassword) {
        waitForVisibility(loginPrompt);
        //loginPrompt.click();
        lightClick(driver, loginPrompt);
        waitForVisibility(signinPopup);
        loginInput.sendKeys(userLogin);
        passInput.sendKeys(userPassword);
        //lightClick(driver, submitButton);
        submitButton.click();
        waitForLoad(driver);
        return new CloudLoggedinPage(driver);
    }
}
