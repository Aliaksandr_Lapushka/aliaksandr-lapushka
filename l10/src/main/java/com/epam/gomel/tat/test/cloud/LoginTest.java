package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.framework.utils.CloudData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends CloudBaseTest {

    @Test(description = "Checks login with valid credentials", groups = "login pos")
    public void validLogin() {
        Assert.assertEquals(cloudLoggedinPage.getUsername(), CloudData.LOGIN_CLOUD);
    }
}
