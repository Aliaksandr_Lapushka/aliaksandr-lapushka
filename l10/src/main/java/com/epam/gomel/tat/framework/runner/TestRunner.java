package com.epam.gomel.tat.framework.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();

        List<String> files = Arrays.asList(
                "./src/main/resources/suite/cloud_test.xml"
        //"./src/main/java/com/epam/gomel/homework/suite/cloud_test.xml"

        );

        testNG.setTestSuites(files);
        testNG.run();
    }
}
