package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.page.cloud.CloudLoggedinPage;
import com.epam.gomel.tat.page.cloud.LoginPage;
import com.epam.gomel.tat.framework.utils.CloudData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.epam.gomel.tat.framework.utils.CloudData.DEF_IMPLICIT_TIMEOUT;

public class CloudBaseTest {

    public static WebDriver driver;
    public static LoginPage loginPage;
    public CloudLoggedinPage cloudLoggedinPage;

    static {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeClass(description = "Chrome browser launch")
    protected void setUp() throws InterruptedException {
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("prefs", chromePrefs);
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        driver = new ChromeDriver(cap);
        driver.manage().timeouts().implicitlyWait(DEF_IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        loginPage = new LoginPage(driver);
        loginPage.open();
        cloudLoggedinPage = loginPage.login(CloudData.LOGIN_CLOUD, CloudData.PASSWORD_CLOUD);
    }

    @AfterClass(description = "Cookies clean up")
    protected void cleanUp() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass(description = "Close driver")
    protected void tearDown() {
        driver.quit();
        //driver.close();
    }
}
