package com.epam.gomel.tat.service.cloud;

import org.apache.commons.lang3.RandomStringUtils;

public class FolderNameService {

    private static final int NAME_SIZE = 4;

    public static String createFolderName() {
        String folderName = "folder_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);

        return folderName;
    }
}
