package com.epam.gomel.tat.page.cloud;

import com.epam.gomel.tat.framework.utils.CloudData;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class CloudLoggedinPage extends PageObject {

    public static final String ACCOUNT_NAME_LOCATOR = "//i[@id='PH_user-email']";
    public static final String CREATE_BUTTON_LOCATOR =
            "(//div[@class='b-toolbar__item b-toolbar__item_create-document-dropdown'])[1]";
    public static final String CREATE_FOLDER_LOCATOR = "(//a[@data-name='folder'])[1]";
    public static final String REMOVE_BUTTON_LOCATOR =
            "//div[@data-name='remove' and contains(@class, 'b-toolbar__btn b-toolbar__btn_remove')]";
    public static final String SUBMIT_DELETE_LOCATOR = ".layer_remove [data-name='remove']";
    public static final String UPLOAD_BUTTON_LOCATOR = "(//div[@class='b-toolbar__btn'][@data-name='upload'])[1]";
    public static final String PUBLISH_LOCATOR = "a[data-name='publish']";
    public static final String COPY_LINK_BUTTON_LOCATOR = "//button[@data-name='copy']";
    //public static final String EXISTING_CONTENT_LOCATOR = "//div[@id='datalist']//div[@data-name = 'link']";
    public static final String EXISTING_ELEMENT_LOCATOR_TEMPLATE = "//div[@id='datalist']"
            + "//div[@data-name = 'link'][@data-id='/%s']";
    public static final String CLOSE_GOTO_TRASH_LOCATOR =
            "//div[@class='layer_trashbin-tutorial']//button[@data-name='close']";
    public static final String FILE_INPUT_LOCATOR = "//input[@class='drop-zone__input']";
    public static final By EXISTING_CONTENT = By.xpath("//div[@id='datalist']//div[@data-name = 'link']");
    public static final By CHECKBOX_LOCATOR = By.className("b-checkbox__box");
    //public static final By ATTACH_BUTTON_LOCATOR = By
    //.xpath("//div[@class='layer_upload__controls__btn-wrapper js-fileapi-wrapper']");
    public static final By CONFIRM_DRAG_AND_DROP_BUTTON_LOCATOR = By.xpath("//button[@data-name='move']");

    @FindBy(how = How.XPATH, using = CREATE_BUTTON_LOCATOR)
    private WebElement createButton;

    @FindBy(how = How.XPATH, using = CREATE_FOLDER_LOCATOR)
    private WebElement createFolder;

    @FindBy (how = How.XPATH, using = UPLOAD_BUTTON_LOCATOR)
    private WebElement uploadFileButton;

    @FindBy(how = How.XPATH, using = REMOVE_BUTTON_LOCATOR)
    private WebElement removeButton;

    @FindBy(className = "layer__input")
    private WebElement folderNameInput;

    @FindBy(how = How.XPATH, using = PUBLISH_LOCATOR)
    private WebElement publishItem;

    @FindBy(how = How.XPATH, using = ACCOUNT_NAME_LOCATOR)
    private WebElement accountName;

    public CloudLoggedinPage(WebDriver driver) {
        super(driver);
    }

    public String getUsername() {
        String login;
        waitForReadiness(By.xpath(ACCOUNT_NAME_LOCATOR));
        login = accountName.getText();
        login = login.split("@")[0];
        return login;
    }

    public void createRemoteFolder(String folderName) {
        waitForVisibility(createButton);
        lightClick(driver, createButton);
        //createButton.click();
        waitForVisibility(createFolder);
        lightClick(driver, createFolder);
        //createFolder.click();
        waitForVisibility(folderNameInput);
        folderNameInput.sendKeys(folderName);
        //waitForVisibility(addFolder);
        folderNameInput.sendKeys(Keys.ENTER);
        //Thread.sleep(CloudData.DEF_THREAD_SLEEP);
        waitForLoad(driver);
    }

    public void delete() throws InterruptedException {
        try {
            waitForVisibility(removeButton);
            //removeButton.click();
            lightClick(driver, removeButton);
            Thread.sleep(CloudData.DEF_THREAD_SLEEP);
            WebElement submit = driver.findElement(By.cssSelector(SUBMIT_DELETE_LOCATOR));
            //submit.click();
            lightClick(driver, submit);
            By by = By.xpath(CLOSE_GOTO_TRASH_LOCATOR);
            waitForReadiness(by);
            WebElement closePopup = driver.findElement(by);
            Thread.sleep(CloudData.DEF_THREAD_SLEEP);
            lightClick(driver, closePopup);
        } catch (NoSuchElementException e) {
            System.out.println("No elements selected");
            e.printStackTrace();
        }
        Thread.sleep(CloudData.DEF_THREAD_SLEEP);
    }

    public void checkExactElement(String name) throws Exception {
        if (isElementPresent(name)) {
            WebElement wrap = getExactElement(name);
            WebElement innerCheckbox = wrap.findElement(CHECKBOX_LOCATOR);
            //innerCheckbox.click();
            waitForVisibility(innerCheckbox);
            Actions actions = new Actions(driver);
            actions.moveToElement(innerCheckbox);
            actions.build().perform();
            lightClick(driver, innerCheckbox);
        } else {
            throw new Exception("No such element " + name);
        }
    }

    public boolean isElementPresent(String name) {
        String resultingLocator = String.format(EXISTING_ELEMENT_LOCATOR_TEMPLATE, name);
        By by = By.xpath(resultingLocator);
        waitForReadiness(by);
        boolean result = driver.findElement(by).isDisplayed();
        return result;
    }

    public boolean isDeleted(String name) {
        String resultingLocator = String.format(EXISTING_ELEMENT_LOCATOR_TEMPLATE, name);
        By by = By.xpath(resultingLocator);
        waitForLoad(driver);
        return driver.findElements(by).isEmpty();
    }

    public boolean isEmptyCloud() {
        waitForLoad(driver);
        boolean result = false;
        List<WebElement> listOfElements = driver.findElements(EXISTING_CONTENT);
        if (listOfElements.isEmpty()) {
            result = true;
        }
        return result;
    }

    public String randomElement() {
        //WebElement element = null;
        String elementName;
        List<WebElement> listOfElements = driver.findElements(EXISTING_CONTENT);
        int index = (new Random()).nextInt(listOfElements.size());
        return elementName = listOfElements.get(index).getAttribute("data-id").substring(1);
    }

    public WebElement getExactElement(String name) {
        WebElement element = null;
        String locator = String.format(EXISTING_ELEMENT_LOCATOR_TEMPLATE, name);
        waitForReadiness(By.xpath(locator));
        //System.out.println("getExactElement " + locator);
        //waitForReadiness(by);
        return element = driver.findElement(By.xpath(locator));
    }

    public void uploadFile(String path) {
        waitForVisibility(uploadFileButton);
        Actions actions = new Actions(driver);
        actions.moveToElement(uploadFileButton);
        actions.click();
        actions.build().perform();
        WebElement fileInput = driver.findElement(By.xpath(FILE_INPUT_LOCATOR));
        fileInput.sendKeys(path);
    }

    public void dragNdrop(WebElement file, WebElement folder) {
        Actions actions = new Actions(driver);
        actions.moveToElement(file);
        waitForClickability(file);
        actions.dragAndDrop(file, folder);
        actions.build().perform();
        waitForReadiness(CONFIRM_DRAG_AND_DROP_BUTTON_LOCATOR);
        WebElement confirm = driver.findElement(CONFIRM_DRAG_AND_DROP_BUTTON_LOCATOR);
        lightClick(driver, confirm);
    }

    public FolderPage openFolder(String name) {
        String resultingLocator = String.format(EXISTING_ELEMENT_LOCATOR_TEMPLATE, name);
        waitForReadiness(By.xpath(resultingLocator));
        lightClick(driver, driver.findElement(By.xpath(resultingLocator)));
        return new FolderPage(driver);
    }

    public String getPublicLink(WebElement element) throws UnsupportedFlavorException, IOException {
        Actions actions = new Actions(driver);
        waitForVisibility(element);
        actions.moveToElement(element);
        actions.contextClick(element);
        actions.build().perform();
        waitForReadiness(By.cssSelector(PUBLISH_LOCATOR));
        WebElement elementOpen = driver.findElement(By.cssSelector(PUBLISH_LOCATOR));
        //elementOpen.click();
        lightClick(driver, elementOpen);
        //waitForVisibility(copyLink);
        waitForReadiness(By.xpath(COPY_LINK_BUTTON_LOCATOR));
        //copyLink.click();
        lightClick(driver, driver.findElement(By.xpath(COPY_LINK_BUTTON_LOCATOR)));
        waitForLoad(driver);
        String data = (String) Toolkit.getDefaultToolkit()
                .getSystemClipboard().getData(DataFlavor.stringFlavor);
        return data;
    }
}
