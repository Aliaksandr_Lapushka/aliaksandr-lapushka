package com.epam.gomel.tat.framework.utils;

public class TestParam {
    private static String testName;

    public static String getTestName() {
        return testName;
    }

    public static void setTestName(String name) {
        TestParam.testName = name;
    }
}
