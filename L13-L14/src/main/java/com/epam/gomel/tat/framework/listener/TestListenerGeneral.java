package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestResult;

public class TestListenerGeneral extends TestListenerLogin {
    public void onTestSuccess(ITestResult tr) {
        Log.info("[TEST PASSED] " + tr.getName());
    }

    public void onFinish(ITestContext testContext) {
        Log.info("[TEST FINISHED] " + testContext.getName());
        Log.debug("Closing browser");
        Browser.getInstance().stopBrowser();
    }
}
