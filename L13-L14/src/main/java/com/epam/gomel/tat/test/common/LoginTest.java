package com.epam.gomel.tat.test.common;

import com.epam.gomel.tat.bo.Account;
import com.epam.gomel.tat.bo.AccountFactory;
import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.framework.listener.SuiteListenerLogin;
import com.epam.gomel.tat.framework.listener.TestListenerLogin;
import com.epam.gomel.tat.service.common.AuthenticationService;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListenerLogin.class, SuiteListenerLogin.class})
public class LoginTest {

    @Test(description = "Checks login process", testName = "Login with valid credentials", groups = "login pos")
    public void correctLoginCheck() throws MailRuAuthenticationException {
        Account account = AccountFactory.getExistentAccount();
        AuthenticationService authenticationService = new AuthenticationService();
        authenticationService.loginUsingUsername(account);
        Assert.assertTrue(authenticationService.isLoggedIn(account));
        /*
        * Though using multiple asserts in one test is an anti-pattern I decided that in this particular case
        * it would be appropriate for time saving as cloudLoginCheck() (in comment below) is very similar to this method
        */
        Assert.assertTrue(authenticationService.isRedirectedToCloud(account));
    }

    /*
    * @Test(description = "Checks redirection to cloud", dependsOnMethods = "correctLoginCheck")
    * public void cloudLoginCheck() throws MailRuAuthenticationException {
    *    Account account = AccountFactory.getExistentAccount();
    *    AuthenticationService authenticationService = new AuthenticationService();
    *    Assert.assertTrue(authenticationService.isRedirectedToCloud(account));
    * }
    */

    @Test(description = "Checks incorrect login exception", groups = "login neg",
            expectedExceptions = MailRuAuthenticationException.class)
    public void incorrectLoginCheck() throws MailRuAuthenticationException {
        Account account = AccountFactory.getInvalidAccount();
        AuthenticationService authenticationService = new AuthenticationService();
        authenticationService.loginUsingUsername(account);
    }
}
