package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.reporting.Log;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileService extends AbstractCloudService {

    private static final int NAME_SIZE = 5;
    private static final int STRING_SIZE = 10;

    private String fileName;
    private File file;

    public String getFileName() {
        return fileName;
    }

    public String createFile() {
        fileName = RandomStringUtils.randomAlphanumeric(NAME_SIZE) + ".txt";
        //Log.info("Creating file on local disk: " + PATH_FOR_UPLOADING + fileName);
        Log.info("Creating file on local disk: " + fileName);
        file = new File(fileName);
        try (FileWriter writer = new FileWriter(file, false)) {
            writer.write(RandomStringUtils.randomAlphabetic(STRING_SIZE));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return fileName;
    }

    public void uploadFile() {
        //cloudLoggedinPage = new LoggedinPageCloud();
        Log.info("Uploading file to Cloud...");
        cloudLoggedinPage.uploadFile(file.getAbsolutePath());
    }

    public boolean isFileExists() {
        Log.info("Verifying if file exists in Cloud...");
        return super.isElementExists(fileName);
    }

    public void deleteFile() {
        Log.info("Removing file from local disk...");
        try {
            Files.delete(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
