package com.epam.gomel.tat.bo;

public class Account {

    private String email;
    private String username;
    private String password;

    public Account(String email, final String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public Account() {
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
