package com.epam.gomel.tat.page.mail;

import com.epam.gomel.tat.framework.ui.Element;
import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

/**
 * Created by Aliaksandr Lapushka on 24.07.2017.
 */
public class LoggedInPageMail extends AbstractPageMail {

    private static final Element accountName = new Element(cssSelector("#PH_user-email"));
    private static final Element newLetterButton = new Element(cssSelector("a[data-name='compose']"));
    private static final Element toOutbox = new Element(cssSelector("a[href='/messages/sent/']"));
    private static final Element openCloud = new Element(xpath("//span[@data-name='ph-cloud']"));
    private static final String  LETTER_SUBJECT_LOCATOR_TEMPLATE =
            "//a[@data-subject]//div[@class='b-datalist__item__subj'][contains(text(),'%s')]";

    public void accountNamePending() {
        accountName.waitForAppear();
    }

    public String getAccountName() {
        return accountName.getText();
    }

    public ComposeLetterPage composeNewLetter() {
        newLetterButton.waitForAppear();
        newLetterButton.click();
        return new ComposeLetterPage();
    }

    public LoggedinPageCloud toCloud() {
        openCloud.waitForAppear();
        openCloud.click();
        return new LoggedinPageCloud();
    }

    public SentPageMail toOutbox() {
        toOutbox.waitForAppear();
        toOutbox.click();
        return new SentPageMail();
    }

    public boolean isLetterPresent(String subject) {
        return super.isLetterPresent(LETTER_SUBJECT_LOCATOR_TEMPLATE, subject);
    }
}
