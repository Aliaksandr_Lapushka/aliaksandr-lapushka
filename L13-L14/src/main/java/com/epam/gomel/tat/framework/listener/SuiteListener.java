package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.framework.runner.Parameters;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {
    @Override
    public void onStart(ISuite iSuite) {
        iSuite.getXmlSuite().setParallel(Parameters.instance().getParallelMode());
        iSuite.getXmlSuite().setThreadCount(Parameters.instance().getThreadCount());
        Log.info("[SUITE STARTED] " + iSuite.getName());
    }

    @Override
    public void onFinish(ISuite iSuite) {
        Log.info("[SUITE FINISHED] " + iSuite.getName());
        //Log.debug("Quit driver, closing windows...");
    }
}
