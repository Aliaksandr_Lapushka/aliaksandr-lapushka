package com.epam.gomel.tat.service.common;

import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.bo.Account;
import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;
import com.epam.gomel.tat.page.mail.LoggedInPageMail;
import com.epam.gomel.tat.page.mail.LoginPage;
import org.openqa.selenium.*;

public class AuthenticationService {

    //private WebDriver driver;
    private LoggedInPageMail loggedInPage;
    private LoggedinPageCloud cloudLoggedinPage;

    public void loginUsingUsername(Account account) throws MailRuAuthenticationException {
        LoginPage loginPage = new LoginPage();
        loginPage.open();
        Log.info("Logging user: " + account.getEmail());
        loginPage.enterLogin(account.getEmail());
        loginPage.enterPassword(account.getPassword());
        //loginPage.submit();
        loggedInPage = loginPage.submit();
        loggedInPage = new LoggedInPageMail();
        try {
            loggedInPage.accountNamePending();
        } catch (TimeoutException e) {
            Log.error("Login attempt failed", e);
            throw new MailRuAuthenticationException("Login failed");
        }
    }

    public boolean isLoggedIn(Account account) {
        Log.info(String.format("Evaluating if user logged in. Matching '%1$s' vs '%2$s'", account.getEmail(),
                loggedInPage.getAccountName()));
        return loggedInPage
                .getAccountName()
                .equals(account.getEmail());
    }

    public void openCloudLoggedinPage() {
        Log.info("Opening Mail.ru Cloud...");
        loggedInPage.toCloud();
        cloudLoggedinPage = new LoggedinPageCloud();
    }

    public boolean isRedirectedToCloud(Account account) throws MailRuAuthenticationException {
        //loginUsingUsername(account);
        openCloudLoggedinPage();
        Log.info(String.format("Evaluating if redirected to Cloud. Matching '%1$s' vs '%2$s'", account.getEmail(),
                cloudLoggedinPage.getUsername()));
        return cloudLoggedinPage
                .getUsername()
                .equals(account.getEmail());
    }
}
