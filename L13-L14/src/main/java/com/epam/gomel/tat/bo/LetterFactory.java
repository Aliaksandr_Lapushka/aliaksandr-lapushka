package com.epam.gomel.tat.bo;

import com.epam.gomel.tat.framework.utils.MailRuData;
import org.apache.commons.lang3.RandomStringUtils;

public class LetterFactory {

    private static final int NAME_SIZE = 4;
    private static final int MESSAGE_LINE_SIZE = 4;

    public static Letter getFullLetter() {
        return new Letter(MailRuData.VALID_TEST_LOGIN, createLeterSubject(), createLetterText());
    }

    public static Letter getNoRecipientLetter() {
        return new Letter("", "", "");
    }

    public static Letter getNoSubjectLetter() {
        return new Letter(MailRuData.VALID_TEST_LOGIN, "", "");
    }

    public static Letter getDraftLetter() {
        return new Letter("", createDraftSubject(), "");
    }

    public static String createLeterSubject() {
        String subjName = "Subject_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);

        return subjName;
    }

    public static String createDraftSubject() {
        String subjName = "Draft_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);

        return subjName;
    }

    public static String createLetterText() {
        String text = "Line " + RandomStringUtils.randomAlphabetic(MESSAGE_LINE_SIZE);
        return text;
    }
}
