package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;
import org.openqa.selenium.WebDriver;

public abstract class AbstractCloudService {

    protected LoggedinPageCloud cloudLoggedinPage = new LoggedinPageCloud();
    protected WebDriver driver;

    public boolean isElementExists(String name) {
        Log.info("Looking for cloud content: " + name);
        return cloudLoggedinPage.isElementPresent(name);
    }
}
