package com.epam.gomel.tat.service.mail;

import com.epam.gomel.tat.bo.Letter;
import com.epam.gomel.tat.exception.MailRuNoRecipientException;
import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.framework.utils.MailFolder;
import com.epam.gomel.tat.page.mail.*;

public class LetterService {

    private LoggedInPageMail loggedInPage;
    private ComposeLetterPage composeLetterPage;
    private SentPageMail sentPage;
    private DraftsPageMail draftsPage;
    private TrashPageMail trashPage;
    private LetterSentPage letterSentPage;

    public void sendLetter(Letter letter) throws MailRuNoRecipientException {
        Log.info(String.format("Sending letter to '%1$s' with subject '%2$s'", letter.getRecipient(),
                letter.getSubject()));
        loggedInPage = new LoggedInPageMail();
        composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.enterRecipient(letter.getRecipient());
        composeLetterPage.enterSubject(letter.getSubject());
        composeLetterPage.enterText(letter.getText());
        letterSentPage = composeLetterPage.sendLetter();
    }

    public void saveAsDraft(Letter letter) {
        Log.info("Saving draft with subject: " + letter.getSubject());
        loggedInPage = new LoggedInPageMail();
        composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.enterSubject(letter.getSubject());
        composeLetterPage.saveDraft();
        draftsPage = composeLetterPage.toDrafts();
    }

    public void deleteFromDrafts(Letter letter) {
        draftsPage = composeLetterPage.toDrafts();
        Log.info("Deleting draft with subject: " + letter.getSubject());
        draftsPage.checkExactDraft(letter.getSubject());
        draftsPage.deleteDraft();
    }

    public void draftPermanentDelete(Letter letter) {
        Log.info("Permanent deleting draft with subject: " + letter.getSubject());
        trashPage = draftsPage.openTrash();
        trashPage.checkExactLetter(letter.getSubject());
        trashPage.permanentDelete();
    }

    public boolean isLetterExists(Letter letter, MailFolder folder) {
        Log.info(String.format("Looking for letter with subject '%1$s' in folder '%2$s'", letter.getSubject(), folder));
        boolean result;
        switch (folder) {

            case INBOX:
                loggedInPage = letterSentPage.redirectToInbox();
                result = loggedInPage.isLetterPresent(letter.getSubject());
                break;

            case OUTBOX:
                sentPage = loggedInPage.toOutbox();
                result = sentPage.isLetterPresent(letter.getSubject());
                break;

            case DRAFTS:
                result = draftsPage.isLetterPresent(letter.getSubject());
                break;

            case TRASH:
                result = trashPage.isLetterPresent(letter.getSubject());
                break;

            default:
                result = false;
        }
        return result;
    }
}
