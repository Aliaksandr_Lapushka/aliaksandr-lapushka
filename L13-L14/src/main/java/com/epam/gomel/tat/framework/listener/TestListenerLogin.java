package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListenerLogin implements ITestListener {

    public void onStart(ITestContext testContext) {
        Log.info("[TEST STARTED] " + testContext.getName());
    }

    public void onFinish(ITestContext testContext) {
        Log.info("[TEST FINISHED] " + testContext.getName());
    }

    public void onTestFailure(ITestResult tr) {
        Log.error("[TEST FAILED] " + tr.getName(), tr.getThrowable());
        Browser.getInstance().screenshot();
        //Browser.getInstance().stopBrowser();
    }

    public void onTestSkipped(ITestResult tr) {
        Log.info("[TEST SKIPPED] " + tr.getName());
    }

    public void onTestSuccess(ITestResult tr) {
        Log.info("[TEST PASSED] " + tr.getName());
        Browser.getInstance().stopBrowser();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        Log.info("[LAUNCHING TEST METHOD]" + iTestResult.getName());
    }
}
