package com.epam.gomel.tat.test.mail;

import com.epam.gomel.tat.bo.LetterFactory;
import com.epam.gomel.tat.exception.MailRuNoRecipientException;
import com.epam.gomel.tat.framework.utils.MailFolder;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NoSubjectLetterTest extends MailBaseTest {

    @Test(description = "Check empty subject letter forwarding")
    public void sendNoSubjectLetterCheck() throws MailRuNoRecipientException {
        letter = LetterFactory.getNoSubjectLetter();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterExists(letter, MailFolder.INBOX));
    }

    @Test(description = "Check empty subject letter was saved in outbox", dependsOnMethods = "sendNoSubjectLetterCheck")
    public void sendNoSubjectLetterCheckOutbox() {
        Assert.assertTrue(letterService.isLetterExists(letter, MailFolder.OUTBOX));
    }
}
