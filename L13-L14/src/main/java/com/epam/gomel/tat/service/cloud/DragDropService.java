package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.reporting.Log;

public class DragDropService extends AbstractCloudService {

    public void dragDrop(String filename, String foldername) {
        Log.info(String.format("Dragging '%1$s' into folder '%2$s'", filename, foldername));
        cloudLoggedinPage.dragDrop(filename, foldername);
    }
}
