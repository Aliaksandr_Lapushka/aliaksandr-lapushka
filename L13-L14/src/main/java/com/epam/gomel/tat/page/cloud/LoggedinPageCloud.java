package com.epam.gomel.tat.page.cloud;

import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.framework.ui.Element;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

public class LoggedinPageCloud extends AbstractPageCloud {

    private static final Element accountName = new Element(xpath("//i[@id='PH_user-email']"));
    private static final Element createButton =
        new Element(xpath("(//div[@class='b-toolbar__item b-toolbar__item_create-document-dropdown'])[1]"));
    private static final Element createFolder = new Element(xpath("(//a[@data-name='folder'])[1]"));
    private static final Element folderNameInput = new Element(cssSelector(".layer__input"));
    private static final Element createFolderSubmit = new Element(xpath("//button[@data-name='add']"));
    private static final Element deleteButton =
        new Element(xpath("//div[@data-name='remove' and contains(@class, 'b-toolbar__btn b-toolbar__btn_remove')]"));
    private static final Element submitDelete = new Element(cssSelector(".layer_remove [data-name='remove']"));
    private static final Element uploadButton =
        new Element(xpath("(//div[@class='b-toolbar__btn'][@data-name='upload'])[1]"));
    private static final Element uploadHeader = new Element(xpath("//span[@class='layer_upload__title__folder']"));
    private static final Element fileInput =
        new Element(xpath("//form[@class='layer__form']//input[@class='drop-zone__input']"));
    private static final Element publishButton = new Element(cssSelector("a[data-name='publish']"));
    private static final Element copylinkButton = new Element(xpath("//button[@data-name='copy']"));
    private static final Element mainPage =
        new Element(xpath("//div[@class='pm-menu__left']"));
    private static final Element confirmDragDropButton = new Element(xpath("//button[@data-name='move']"));
    private static final Element toTrashPopupDismiss =
        new Element(xpath("//div[@class='layer_trashbin-tutorial']//button[@data-name='close']"));

    private static final By EXISTING_CONTENT = By.xpath("//div[@id='datalist']//div[@data-name = 'link']");

    private static final String CHECKBOX_LOCATOR_TEMPLATE = "//div[@id='datalist']//div[@data-name = 'link']"
            + "[@data-id='/%s']//*[@class = 'b-checkbox__box']";

    public String getUsername() {
        accountName.waitForAppear();
        return accountName.getText();
    }

    public void createRemoteFolder(String folderName) {
        createButton.waitForAppear(super.PENDING_TIMEOUT);
        createButton.click();
        createFolder.waitForAppear();
        createFolder.click();
        folderNameInput.waitForAppear();
        folderNameInput.type(folderName);
        createFolderSubmit.waitForAppear();
        createFolderSubmit.click();
        createButton.waitForAppear();
        super.isElementPresent(folderName);
    }

    public void delete() {
        deleteButton.waitForAppear(super.PENDING_TIMEOUT);
        deleteButton.click();
        submitDelete.waitForAppear();
        submitDelete.click();
        toTrashPopupDismiss.waitForAppear();
        toTrashPopupDismiss.click();
    }

    public void checkExactElement(String name) throws Exception {
        if (isElementPresent(name)) {
            String checkBoxResLocator = String.format(CHECKBOX_LOCATOR_TEMPLATE, name);
            Element checkbox = new Element(xpath(checkBoxResLocator));
            checkbox.waitForAppear();
            Actions actions = new Actions(Browser.getInstance().getWrappedDriver());
            actions.moveToElement(Browser.getInstance().getWrappedDriver().findElement(By.xpath(checkBoxResLocator)));
            actions.build().perform();
            checkbox.click();
        } else {
            throw new Exception("No such element " + name);
        }
    }

    public boolean isElementPresent(String name) {
        return super.isElementPresent(name);
    }

    public void uploadFile(String path) {
        uploadButton.waitForAppear();
        uploadButton.click();
        uploadHeader.waitForAppear();
        //fileInput.waitForAppear();
        fileInput.uploadFile(path);
    }

    public boolean isEmptyCloud() {
        boolean result = false;
        List<WebElement> listOfElements = Browser.getInstance().getWrappedDriver().findElements(EXISTING_CONTENT);
        if (listOfElements.isEmpty()) {
            result = true;
        }
        return result;
    }

    public String getRandomElementName() {
        List<WebElement> listOfElements = Browser.getInstance()
                .getWrappedDriver()
                .findElements(EXISTING_CONTENT);
        int index = (new Random()).nextInt(listOfElements.size());
        return listOfElements.get(index).getAttribute("data-id").substring(1);
    }

    public void dragDrop(String fileName, String folderName) {
        Actions actions = new Actions(Browser.getInstance().getWrappedDriver());
        String fileReslocator = String.format(super.EXISTING_ELEMENT_LOCATOR_TEMPLATE, fileName);
        String folderReslocator = String.format(super.EXISTING_ELEMENT_LOCATOR_TEMPLATE, folderName);
        WebElement file = Browser.getInstance().findElement(By.xpath(fileReslocator));
        WebElement folder = Browser.getInstance().findElement(By.xpath(folderReslocator));
        actions.moveToElement(file);
        actions.dragAndDrop(file, folder);
        actions.build().perform();
        confirmDragDropButton.waitForAppear(super.PENDING_TIMEOUT);
        confirmDragDropButton.click();
    }

    public void openFolder(String name) {
        String resultingLocator = String.format(super.EXISTING_ELEMENT_LOCATOR_TEMPLATE, name);
        Element folder = new Element(xpath(resultingLocator));
        folder.click();
    }

    public void goToMain() {
        mainPage.waitForAppear();
        mainPage.click();
    }

    public String getPublicLink(String  elementName) throws UnsupportedFlavorException, IOException {
        Actions actions = new Actions(Browser.getInstance().getWrappedDriver());
        WebElement element = Browser.getInstance()
                .getWrappedDriver()
                .findElement(By.xpath(super.getElementXpath(elementName)));
        actions.moveToElement(element);
        actions.contextClick(element);
        actions.build().perform();
        publishButton.waitForAppear(super.PENDING_TIMEOUT);
        publishButton.click();
        copylinkButton.waitForAppear();
        copylinkButton.click();
        return (String) Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .getData(DataFlavor.stringFlavor);
    }
}
