package com.epam.gomel.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.gomel.tat.framework.reporting.Log;
import org.testng.TestNG;

public class Runner {

    public static TestNG configTestNG() {
        TestNG tng = new TestNG();
        tng.setTestSuites(Parameters.instance().getSuites());
        return tng;
    }

    public static void main(String[] args) {
        Log.info("Parse CLI");
        parseCli(args);
        Log.info("Launching application...");
        configTestNG().run();
        Log.info("Shutting down application...");
    }

    private static void parseCli(String[] args) {
        Log.info("Parsing CLIs with JCommander");
        JCommander jc = new JCommander(Parameters.instance());
        try {
            jc.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage(), e);
            jc.usage();
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jc.usage();
            System.exit(0);
        }
    }
}
