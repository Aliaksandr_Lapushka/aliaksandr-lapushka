package com.epam.gomel.tat.framework.utils;

public enum MailFolder {
    INBOX,
    OUTBOX,
    DRAFTS,
    TRASH
}
