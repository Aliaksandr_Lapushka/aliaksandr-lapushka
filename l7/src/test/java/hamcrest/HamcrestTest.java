package hamcrest;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static hamcrest.GirlMatcher.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Aliaksandr Lapushka on 17.07.2017.
 */
public class HamcrestTest {

    Girl aGirl;
    Boy mockedBoy;

    @BeforeClass
    public void setUp() {
        mockedBoy = mock(Boy.class);
        aGirl = new Girl(true, true, mockedBoy);
        when(mockedBoy.isRich()).thenReturn(true);
    }

    @Test (description = "Checks that a girl is pretty")
    public void isPrettyTest() {
        assertThat(aGirl, both(pretty()).and(hasRichBF()).and(willGetShoes()).and(hasExcellentMood())
               );
    }
}
