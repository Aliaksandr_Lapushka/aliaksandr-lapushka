package hamcrest;

import com.epam.gomel.homework.*;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import static org.hamcrest.core.Is.is;

/**
 * Created by Aliaksandr Lapushka on 17.07.2017.
 */
public class GirlMatcher {

    public static Matcher<Girl> pretty() {
        return new FeatureMatcher<Girl, Boolean>(is(true), "Girlfriend should be pretty",
                "Pretty -") {
            protected Boolean featureValueOf(Girl girl) {
                return girl.isPretty();
            }
        };
    }

    public static Matcher<Girl> hasRichBF() {
        return new FeatureMatcher<Girl, Boolean>(is(true), "Boyfriend should be rich",
                "Rich - ") {
            protected Boolean featureValueOf(Girl girl) {
                return girl.isBoyfriendRich();
            }
        };
    }

    public static Matcher<Girl> willGetShoes() {
        return new FeatureMatcher<Girl, Boolean>(is(true), "Girl would get a new shoes",
                "New shoes - ") {
            protected Boolean featureValueOf(Girl girl) {
                return girl.isBoyFriendWillBuyNewShoes();
            }
        };
    }

    public static Matcher<Girl> hasExcellentMood() {
        return new FeatureMatcher<Girl, Mood>(is(Mood.EXCELLENT), "Girl has excellent mood",
                "Mood - ") {
            protected Mood featureValueOf(Girl girl) {
                return girl.getMood();
            }
        };
    }
}
