package factory;

import com.epam.gomel.homework.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

/**
 * Created by Aliaksandr Lapushka on 15.07.2017.
 */
public class FactoryTest {

    @DataProvider(name = "Data for Boy getMood() test")
    protected Object[][] createBoyData() {
        return new Object[][] {
                {Mood.EXCELLENT, Month.AUGUST, 2_000_000, "yes"},
                {Mood.GOOD, Month.MAY, 3_000_000, "yes"},
                {Mood.NEUTRAL, Month.JUNE, 2_500_000, "no"},
                {Mood.BAD, Month.JULY, 500, "no"},
                {Mood.BAD, Month.JANUARY, 4_000_000, "no"},
                {Mood.BAD, Month.FEBRUARY, 100_000, "yes"},
                {Mood.HORRIBLE, Month.OCTOBER, 500, "no"},
                {Mood.HORRIBLE, Month.OCTOBER, 0, "no"}
        };
    }

    @Factory(dataProvider = "Data for Boy getMood() test")
    public Object[] createBoyFactoryTest(Mood mood, Month month, double wealth,
                                          String gF) {
        Girl girlFriend = null;
        if (gF.equals("yes")) {
            girlFriend = new Girl(true, true);
        }
        return new Object[] {
                new BoyFactory(mood, month, wealth, girlFriend)
        };
    }
}
