package factory;

import com.epam.gomel.homework.*;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Aliaksandr Lapushka on 15.07.2017.
 */
public class BoyFactory {

    protected Boy fBoy;
    protected Girl gF;
    protected Mood expectedMood;
    protected Month month;
    protected double wealth;

    public BoyFactory(Mood expectedMood, Month month, double wealth, Girl gF) {
        this.expectedMood = expectedMood;
        this.month = month;
        this.wealth = wealth;
        this.gF = gF;
    }

    @Test(description = "Tests getMood() result at given preconditions")
    public void getMoodFactoryTest() {
        fBoy = new Boy(month, wealth, gF);
        Mood actual = fBoy.getMood();
        Assert.assertEquals(actual, expectedMood, "Check Boy getMood()");
    }
}
