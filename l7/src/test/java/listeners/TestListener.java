package listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

/**
 * Created by Aliaksandr Lapushka on 16.07.2017.
 */
public class TestListener implements IInvokedMethodListener {

    public void beforeInvocation (IInvokedMethod method, ITestResult testResult) {
        System.out.println("[METHOD_STARTED] - " + method.getTestMethod().getMethodName());
    }

    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(String.format("[METHOD FINISHED] - %s >>> %s", method.getTestMethod().getMethodName(),
                testResult.getStatus() == 1 ? "Success" : "Fail"));
    }
}
