import org.testng.annotations.BeforeClass;
import com.epam.gomel.homework.*;

/**
 * Created by A. Lapushka 14.07.2017.
 */
public class BaseHumanTest {

    protected Boy aBoy;
    protected Girl aGirl;

    @BeforeClass
    public void setUp() {

        aBoy = new Boy(Month.AUGUST, 2_000_000);
        aGirl = new Girl(true, true, aBoy);
    }
}
