package mockito;

import com.epam.gomel.homework.*;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;


/**
 * Created by Aliaksandr Lapushka on 16.07.2017.
 */
public class MockitoTest {

    Girl testGirl;
    Boy mockedBoy;
    static double WEALTH_VALUE = 1_000_000;

    @BeforeClass
    public void setUp() {
        mockedBoy = mock(Boy.class);
        testGirl = new Girl(true, true, mockedBoy);
        when(mockedBoy.isRich()).thenReturn(true);
        when(mockedBoy.getWealth()).thenReturn((double) 5_000_000);
        when(mockedBoy.isPrettyGirlFriend()).thenReturn(testGirl.isPretty());
    }

    @Test(description = "Checks that mocked Boy is rich")
    public void mockedBehaviourTest0() {
        boolean actual = testGirl.isBoyfriendRich();
        Assert.assertTrue(actual, "Wrong mocked behaviour");
    }

    @Test (description = "Checks that mocked boy gf is pretty")
    public void mockedBehaviourTest1() {
        boolean actual = mockedBoy.isPrettyGirlFriend();
        if (!testGirl.isPretty()) {
            Assert.assertFalse(actual, "Wrong mocked behaviour");
        } else {
            Assert.assertTrue(actual, "Wrong mocked behaviour");
        }
    }

    @Test (description = "Checks that boy's wealth is enough to be rich")
    public void mockedBehaviourTest2() {
        boolean actual = false;
        if (mockedBoy.isRich()) {
            actual = mockedBoy.getWealth() >= WEALTH_VALUE;
        }
        Assert.assertTrue(actual, "Wrong mocked behaviour");
    }
}
