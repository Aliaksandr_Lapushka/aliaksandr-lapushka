package runners;

import org.testng.TestNG;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Aliaksandr Lapushka on 16.07.2017.
 */
public class CustomRunner {
    public static void main(String[] args) {

        List<String> files = Arrays.asList(
                "./src/test/suites/human_test.xml"
        );

        TestNG testNG = new TestNG();

        testNG.setTestSuites(files);
        testNG.run();
    }
}
