import com.epam.gomel.homework.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Created by Aliaksandr Lapushka on 16.07.2017.
 */
public class GirlTest extends BaseHumanTest {

    @Test (description = "Checks if Slim Friend Became Fat",
            groups = "Boolean resulting", priority = 0)
    public void isSlimFriendBecameFatTest() {
        boolean actual = aGirl.isSlimFriendBecameFat();
        Assert.assertFalse(actual, "Method isSlimFriendBecameFat() is buggy");
    }

    @Test (description = "Checks that boyfriend will buy her the shoes", groups =
            "Boolean resulting", priority = 0)
    public void isBoyFriendWillBuyNewShoesTest() {
        boolean actual = aGirl.isBoyFriendWillBuyNewShoes();
        Assert.assertTrue(actual, "Method isBoyFriendWillBuyNewShoes is buggy");
    }

    @Test (description = "Checks that boyfirend is rich", groups = "Boolean resulting",
            priority = 0)
    public void isBoyfriendRichTest() {
        boolean actual = aGirl.isBoyfriendRich();
        Assert.assertTrue(actual, "Method isBoyfriendRich() is buggy");
    }

    @Test (description = "Checks that Girl can spend Boys money", priority = 1)
    @Parameters({"amount1"})
    protected void spendBoyFriendMoneyTest(double amount1) {
        double expected = aBoy.getWealth() - amount1;
        aGirl.spendBoyFriendMoney(amount1);
        double actual = aBoy.getWealth();
        Assert.assertEquals(actual, expected, "Method spendBoyFriendMoney is buggy");
    }

    @Test(description = "Checks that Girls mood is EXCELLENT", priority = 2, groups = "Mood")
    public void girlGetExcelMoodTest() {
        Mood actual = aGirl.getMood();
        Assert.assertEquals(actual, Mood.EXCELLENT, "Method Girl getMood() is buggy");
    }

    @Test(description = "Checks Pretty setter", priority = 3)
    public void prettySetterTest() {
        aGirl.setPretty(true);
        boolean actual = aGirl.isPretty();
        Assert.assertTrue(actual);
    }

    @DataProvider(name = "Girl get mood")
    public static Object[][] datForGirlGetMood() {
        return new Object[][]{
                {true, false, Mood.GOOD},
                {false, true, Mood.NEUTRAL},
                {false, false, Mood.I_HATE_THEM_ALL}
        };
    }

    @Test(description = "Checks Girl getMood() method", dataProvider = "Girl get mood", priority = 2, groups = "Mood")
    public void girlGetMoodTest(boolean pretty, boolean slim, Mood mood) {
        Girl testGirl = new Girl(pretty, slim);
        Mood actual = testGirl.getMood();
        Assert.assertEquals(actual, mood, "Girl getMood() method is buggy");
    }

    @Test (description = "Checks Girl 1 arg constructor", priority = 3)
    public void girlCons1Test() {
        Girl testGirl = new Girl(true);
        boolean actual = testGirl.isPretty();
        Assert.assertTrue(actual, "Girl 1 arg constructor is buggy");
    }

    @Test (description = "Checks Girl default constructor", priority = 3)
    public void girlDefConsTest() {
        Girl testGirl = new Girl();
        boolean actual = testGirl.isSlimFriendGotAFewKilos();
        Assert.assertTrue(actual, "Girl default constructor is buggy");
    }
}
