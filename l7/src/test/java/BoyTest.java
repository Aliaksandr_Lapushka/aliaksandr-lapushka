
import com.epam.gomel.homework.*;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 * Created by Aliaksandr Lapushka on 14.07.2017.
 */
public class BoyTest extends BaseHumanTest {

//    @Test (description = "Checks if mood gets mood", groups = "Main")
//    protected void boyGetMoodTest() {
//        Mood actual = aBoy.getMood();
//        Assert.assertEquals(actual, Mood.EXCELLENT, "Check Boy getMood() method");
//    }

    @Test (description = "Checks that the girlfriend is pretty", groups = "Boolean resulting", priority = 0)
    protected void isPrettyGirlFriendTest() {
        boolean actual = aBoy.isPrettyGirlFriend();
        Assert.assertTrue(actual, "Method isPrettyGirlFriend is buggy");
    }

    @Test (description = "Checks that the boy is rich", groups = "Boolean resulting", priority = 1)
    protected void isReachTest() {
        boolean actual = aBoy.isRich();
        Assert.assertTrue(actual, "Method isRich is buggy");
    }

    @Test (description = "Checks that the birthday is in the summer", groups = "Boolean resulting", priority = 0)
    protected void isSummerMonthTest() {
        boolean actual = aBoy.isSummerMonth();
        Assert.assertTrue(actual, "Method isSummerMonth is buggy");
    }

    @DataProvider(name = "valuesPositive")
    public static Object[][] valuesForPosTest() {
        return new Object[][]{
                {1_000_000},
                {100_000},
                {1}
        };
    }
    @Test (description = "Checks that Boy can spend money", groups = "Positive",
            dataProvider = "valuesPositive", dependsOnGroups = "Boolean resulting")
    protected void spendSomeMoneyPosTest(double amount) {
        double actual = aBoy.getWealth() - amount;
        aBoy.spendSomeMoney(amount);
        Assert.assertEquals(actual, aBoy.getWealth(), "Method spendSomeMoney is buggy");
    }

    @Test (description = "Checks that Boy can spend money", groups = "Negative",
            expectedExceptions = RuntimeException.class)
    @Parameters({"amount"})
    protected void spendSomeMoneyNegTest(double amount) {
        aBoy.spendSomeMoney(amount);
    }

    @Test (description = "Boy 1 arg constructor test", priority = 3, groups = "Constructor")
    protected void boyDefConsTest() {
        Boy testBoy = new Boy(Month.JUNE);
        boolean actual = testBoy.isSummerMonth();
        Assert.assertTrue(actual, "Boy 1 argument constructor is buggy");
    }
}
