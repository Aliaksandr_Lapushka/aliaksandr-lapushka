package com.epam.tat;

/**
 * Interface in top of of hierarchy.
 *
 * @version     06.07.2017
 * @author      Akiaksandr Lapushka
 */
public interface Shape {

    int area();

    int perimeter();

    String getPar();
}
