package com.epam.tat;

import java.util.Locale;

/**
 * Class containing static method that accepts given string and returns Shape of
 * required type.
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public abstract class AbstractShapeFinder {

    protected static Shape getShape(String string) {

        Shape shape;

        String[] tokens = string.split(":");

        switch (Shapes.valueOf(tokens[0].toUpperCase(Locale.ENGLISH))) {
            case RECTANGLE:
                shape = new Rectangle(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
                break;

            case CIRCLE:
                shape = new Circle(Integer.parseInt(tokens[1]));
                break;

            case SQUARE:
                shape = new Square(Integer.parseInt(tokens[1]));
                break;

            case TRIANGLE:
                shape = new Triangle(Integer.parseInt(tokens[1]));
                break;

            default:
                shape = null;
        }

        return shape;
    }
}
