package com.epam.tat;

/**
 * Main class containing entry point.
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public class Runner {

    public static void main(String[] args) {

        String className;
        Shape[] shapes = new Shape[args.length];

        for (int i = 0; i < args.length; ++i) {
            shapes[i] = AbstractShapeFinder.getShape(args[i]);
            className = shapes[i].getClass().getSimpleName();

            System.out.println(className + " - " + shapes[i].getPar());
            System.out.println("    Area: " + shapes[i].area() + "\n" + "    Perimeter: "
                                + shapes[i].perimeter() + "\n");
        }
    }
}
