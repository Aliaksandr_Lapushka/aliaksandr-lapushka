package com.epam.tat;

/**
 * This class provides shape specific methods to calculate area and perimeter
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public class Triangle implements Shape {

    private static final int NO_OF_SIDES = 3;
    private static final int DIVIDER = 4;
    private int side;

    public Triangle(int side) {
        this.side = side;
    }

    public String getPar() {
        String par = Integer.toString(side);

        return par;
    }

    public int perimeter() {
        return side * NO_OF_SIDES;
    }

    public int area() {
        return (int) ((side * side * Math.sqrt(NO_OF_SIDES)) / DIVIDER);
    }

}
