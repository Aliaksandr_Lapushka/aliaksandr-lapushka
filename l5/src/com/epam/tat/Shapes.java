package com.epam.tat;

/**
 * Enum type containing allowed shapes
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public enum Shapes {
    TRIANGLE,
    CIRCLE,
    SQUARE,
    RECTANGLE
}
