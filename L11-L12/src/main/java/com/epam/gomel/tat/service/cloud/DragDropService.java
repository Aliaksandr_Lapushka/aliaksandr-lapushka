package com.epam.gomel.tat.service.cloud;

import org.openqa.selenium.WebDriver;

public class DragDropService extends AbstractCloudService {

    public DragDropService(WebDriver driver) {
        super(driver);
    }

    public void dragDrop(String filename, String foldername) {
        cloudLoggedinPage.dragDrop(filename, foldername);
    }
}
