package com.epam.gomel.tat.page.mail;

import com.epam.gomel.tat.framework.ui.Element;

import static org.openqa.selenium.By.xpath;

public class DraftsPageMail extends SentPageMail {

    private static final String DRAFT_LETTER_LOCATOR_TEMPLATE = "//a[@data-subject='%s']";
    private static final String LETTER_CECKBOX_lOCATOR_TEMPLATE = "//a[@data-subject='%s']"
            + "//div[@class='b-checkbox__box']";
    private static final Element deleteButton = new Element(xpath("(//div[@style='']//div[@data-name='remove'])[1]"));
    private static final Element toTrashFolder =
            new Element(xpath("//i[@class='ico ico_folder ico ico_folder_trash']"));

    public boolean isLetterPresent(String subject) {
        //return super.isLetterPresent(DRAFT_LETTER_LOCATOR_TEMPLATE, subject);
        return super.isLetterPresent(subject);
    }

    public void checkExactDraft(String subject) {
        if (isLetterPresent(subject)) {
            String resultingLocator = String.format(LETTER_CECKBOX_lOCATOR_TEMPLATE, subject);
            Element checkbox = new Element(xpath(resultingLocator));
            checkbox.waitForAppear();
            checkbox.click();
        }
    }

    public void deleteDraft() {
        deleteButton.waitForAppear();
        deleteButton.click();
        toTrashFolder.waitForAppear();
    }

    public TrashPageMail openTrash() {
        toTrashFolder.waitForAppear();
        toTrashFolder.click();
        return new TrashPageMail();
    }
}
