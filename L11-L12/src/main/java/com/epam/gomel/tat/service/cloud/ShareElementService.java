package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.ui.Browser;
import org.openqa.selenium.WebDriver;

import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class ShareElementService extends AbstractCloudService {

    private String elementName;

    public ShareElementService(WebDriver driver) {
        super(driver);
    }

    public void shareRandomElementName() throws IOException, UnsupportedFlavorException {
        if (cloudLoggedinPage.isEmptyCloud()) {
            FolderService folderService = new FolderService(driver);
            folderService.createFolder();
            elementName = folderService.getFolderName();
        } else {
            elementName = cloudLoggedinPage.getRandomElementName();
        }
        Browser
                .getInstance()
                .getWrappedDriver()
                .get(cloudLoggedinPage.getPublicLink(elementName));
    }

    public boolean isCorrectElementWindow() {
        return Browser
                .getInstance()
                .getWrappedDriver()
                .getTitle()
                .matches(elementName + ".*");
    }
}
