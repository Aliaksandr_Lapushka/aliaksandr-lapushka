package com.epam.gomel.tat.service.cloud;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileService extends AbstractCloudService {

    private static final int NAME_SIZE = 5;
    private static final int STRING_SIZE = 10;
    private static final String PATH_FOR_UPLOADING = "D:\\";

    private String fileName;

    public FileService(WebDriver driver) {
        super(driver);
    }

    public String getFileName() {
        return fileName;
    }

    public String createFile() {
        fileName = RandomStringUtils.randomAlphanumeric(NAME_SIZE) + ".txt";
        File file = new File(PATH_FOR_UPLOADING, fileName);
        try (FileWriter writer = new FileWriter(file, false)) {
            writer.write(RandomStringUtils.randomAlphabetic(STRING_SIZE));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return fileName;
    }

    public void uploadFile() {
        //cloudLoggedinPage = new LoggedinPageCloud();
        cloudLoggedinPage.uploadFile(PATH_FOR_UPLOADING + fileName);
    }

    public boolean isFileExists() {
        return super.isElementExists(fileName);
    }

    public void deleteFile() {
        try {
            Files.delete(Paths.get(PATH_FOR_UPLOADING + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
