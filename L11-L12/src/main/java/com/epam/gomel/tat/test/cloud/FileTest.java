package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.service.cloud.DragDropService;
import com.epam.gomel.tat.service.cloud.FileService;
import com.epam.gomel.tat.service.cloud.FolderService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class FileTest extends CloudBaseTest {

    private FileService fileService = new FileService(driver);
    private FolderService folderService = new FolderService(driver);
    private DragDropService dragDropService = new DragDropService(driver);

    @Test(description = "Upload Test")
    public void checkUploadFile() {
        fileService.createFile();
        fileService.uploadFile();
        Assert.assertTrue(fileService.isFileExists());
    }

    @Test(description = "File drag'n'drop test", dependsOnMethods = "checkUploadFile")
    public void checkDragDropFile() {
        folderService.createFolder();
        dragDropService.dragDrop(fileService.getFileName(), folderService.getFolderName());
        folderService.openFolder();
        Assert.assertTrue(fileService.isFileExists());
        folderService.goToRoot();
    }

    @AfterClass(description = "Deleting file")
    public void processGarbage() {
        fileService.deleteFile();
        fileService = null;
        folderService = null;
        dragDropService = null;
    }
}
