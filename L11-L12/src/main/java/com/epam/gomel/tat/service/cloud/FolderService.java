package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

public class FolderService extends AbstractCloudService {

    private static final int NAME_SIZE = 4;

    //private WebDriver driver;
    private String folderName;

    public FolderService(WebDriver driver) {
        super(driver);
    }

    public String getFolderName() {
        return folderName;
    }

    public void createFolder() {
        folderName = "folder_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);
        cloudLoggedinPage = new LoggedinPageCloud();
        cloudLoggedinPage.createRemoteFolder(folderName);
    }

    public void openFolder() {
        cloudLoggedinPage.openFolder(folderName);
    }

    public void goToRoot() {
        cloudLoggedinPage.goToMain();
    }

    public void deleteFolder() throws Exception {
        cloudLoggedinPage.checkExactElement(folderName);
        cloudLoggedinPage.delete();
    }

    public boolean isFolderExists() {
        return super.isElementExists(folderName);
    }
}
