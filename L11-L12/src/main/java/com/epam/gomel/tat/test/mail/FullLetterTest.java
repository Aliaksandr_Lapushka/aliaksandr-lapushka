package com.epam.gomel.tat.test.mail;

import com.epam.gomel.tat.bo.LetterFactory;
import com.epam.gomel.tat.exception.MailRuNoRecipientException;
import com.epam.gomel.tat.framework.utils.MailFolder;
import org.testng.Assert;
import org.testng.annotations.*;

public class FullLetterTest extends MailBaseTest {

    @Test(description = "Check full letter delivery")
    public void sendValidLetterCheck() throws MailRuNoRecipientException {
        letter = LetterFactory.getFullLetter();
        letterService.sendLetter(letter);
        Assert.assertTrue(letterService.isLetterExists(letter, MailFolder.INBOX));
    }

    @Test(description = "Check full letter presence in outbox", dependsOnMethods = "sendValidLetterCheck")
    public void isInOutboxCheck() {
        Assert.assertTrue(letterService.isLetterExists(letter, MailFolder.OUTBOX));
    }
}
