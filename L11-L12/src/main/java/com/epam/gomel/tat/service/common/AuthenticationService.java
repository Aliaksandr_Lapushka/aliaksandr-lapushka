package com.epam.gomel.tat.service.common;

import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.bo.Account;
import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;
import com.epam.gomel.tat.page.mail.LoggedInPageMail;
import com.epam.gomel.tat.page.mail.LoginPage;
import org.openqa.selenium.*;

public class AuthenticationService {

    private WebDriver driver;
    private LoggedInPageMail loggedInPage;
    private LoggedinPageCloud cloudLoggedinPage;

    public AuthenticationService(WebDriver driver) {
        this.driver = driver;
    }

    public void loginUsingUsername(Account account) throws MailRuAuthenticationException {
        LoginPage loginPage = new LoginPage();
        loginPage.open();
        loginPage.enterLogin(account.getEmail());
        loginPage.enterPassword(account.getPassword());
        //loginPage.submit();
        loggedInPage = loginPage.submit();
        loggedInPage = new LoggedInPageMail();
        try {
            loggedInPage.accountNamePending();
        } catch (TimeoutException e) {
            throw new MailRuAuthenticationException("Login failed");
        }
    }

    public boolean isLoggedIn(Account account) {
        return loggedInPage
                .getAccountName()
                .equals(account.getEmail());
    }

    public void openCloudLoggedinPage() {
        loggedInPage.toCloud();
        cloudLoggedinPage = new LoggedinPageCloud();
    }

    public boolean isRedirectedToCloud(Account account) throws MailRuAuthenticationException {
        loginUsingUsername(account);
        openCloudLoggedinPage();
        return cloudLoggedinPage
                .getUsername()
                .equals(account.getEmail());
    }
}
