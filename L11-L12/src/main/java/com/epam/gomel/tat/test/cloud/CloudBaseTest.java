package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.test.mail.MailBaseTest;
import org.testng.annotations.*;

public class CloudBaseTest extends MailBaseTest {

    @BeforeTest
    public void setUp() throws MailRuAuthenticationException {
        super.setUp();
        authenticationService.openCloudLoggedinPage();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    @AfterTest
    public void tearDown() {
        super.tearDown();
    }
}
