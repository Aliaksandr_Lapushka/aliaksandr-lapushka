package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;
import org.openqa.selenium.WebDriver;

public abstract class AbstractCloudService {

    protected LoggedinPageCloud cloudLoggedinPage = new LoggedinPageCloud();
    protected WebDriver driver;

    public AbstractCloudService(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isElementExists(String name) {
        return cloudLoggedinPage.isElementPresent(name);
    }
}
