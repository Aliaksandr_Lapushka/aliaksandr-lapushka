package com.epam.gomel.tat.test.common;

import com.epam.gomel.tat.bo.Account;
import com.epam.gomel.tat.bo.AccountFactory;
import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.service.common.AuthenticationService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest {

    private AuthenticationService authenticationService;

    @BeforeMethod
    public void setUp() {
        Browser browser = Browser.getInstance();
        authenticationService = new AuthenticationService(browser.getWrappedDriver());
    }

    @Test(description = "Checks login process", groups = "login pos")
    public void correctLoginCheck() throws MailRuAuthenticationException {
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginUsingUsername(account);
        Assert.assertTrue(authenticationService.isLoggedIn(account));
    }

    @Test(description = "Checks redirection to cloud", dependsOnMethods = "correctLoginCheck")
    public void cloudLoginCheck() throws MailRuAuthenticationException {
        Account account = AccountFactory.getExistentAccount();
        Assert.assertTrue(authenticationService.isRedirectedToCloud(account));
    }

    @Test(description = "Checks incorrect login exception", groups = "login neg",
            expectedExceptions = MailRuAuthenticationException.class)
    public void incorrectLoginCheck() throws MailRuAuthenticationException {
        Account account = AccountFactory.getInvalidAccount();
        authenticationService.loginUsingUsername(account);
    }

    @AfterMethod
    public void tearDown() {
        Browser.getInstance().stopBrowser();
    }
}
