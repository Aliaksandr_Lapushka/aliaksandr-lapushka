package com.epam.gomel.tat.framework.ui;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

// /Lecture 12 material
public final class Browser implements WrapsDriver {

    private static Browser instance;
    private static final int DEFAULT_TIMEOUT = 3;

    private WebDriver wrappedWebDriver;

    private Browser() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("incognito");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        wrappedWebDriver = new ChromeDriver(options);
        wrappedWebDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wrappedWebDriver.manage().window().maximize();
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public static Browser getInstance() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public void stopBrowser() {
        getInstance().wrappedWebDriver.manage().deleteAllCookies();
        getInstance().wrappedWebDriver.quit();
        getInstance().wrappedWebDriver = null;
        instance = null;
    }

    public void navigate(String url) {
        wrappedWebDriver.get(url);
    }

    public void type(By by, String text) {
        wrappedWebDriver
                .findElement(by)
                .sendKeys(text);
    }

    public void clear(By by) {
        wrappedWebDriver
                .findElement(by)
                .clear();
    }

    public void switchToFrame(By by) {
        wrappedWebDriver.switchTo()
                .frame(wrappedWebDriver.findElement(by));
    }

    public WebElement findElement(By by) {
        return wrappedWebDriver
                .findElement(by);
    }

    public void switchToDefaultContent() {
        wrappedWebDriver.switchTo().defaultContent();
    }

    public String getText(By by) {
        return wrappedWebDriver
                .findElement(by)
                .getText();
    }

    public void uploadFile(String pathToFile, By fileInput) {
        wrappedWebDriver
                .findElement(fileInput)
                .sendKeys(pathToFile);
    }

    //My methods
    public void click(By by) {
        waitForVisibility(by);
        WebElement element = wrappedWebDriver.findElement(by);
        JavascriptExecutor js = (JavascriptExecutor) getWrappedDriver();
        js.executeScript("arguments[0].style.border='4px groove green'", element);
        js.executeScript("arguments[0].style.border=''", element);
        Actions actions = new Actions(wrappedWebDriver);
        actions.moveToElement(element);
        actions.click();
        actions.build().perform();
    }

    public boolean isVisible(By by) {
        return !wrappedWebDriver
                .findElements(by)
                .isEmpty();
    }

    protected void waitForVisibility(By by) {
        new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    protected void waitForVisibility(By by, int timeout) {
        new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForAlert() {
        new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT)
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent());
    }

    public String fetchAlertText() {
        return wrappedWebDriver
                .switchTo()
                .alert()
                .getText();
    }

    public void dismissAlert() {
        wrappedWebDriver
                .switchTo()
                .alert()
                .dismiss();
    }

    public void acceptAlert() {
        wrappedWebDriver
                .switchTo()
                .alert()
                .accept();
    }
}
