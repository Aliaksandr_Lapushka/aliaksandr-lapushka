package com.epam.gomel.tat.test.mail;

import com.epam.gomel.tat.bo.Account;
import com.epam.gomel.tat.bo.AccountFactory;
import com.epam.gomel.tat.bo.Letter;
import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.service.common.AuthenticationService;
import com.epam.gomel.tat.service.mail.LetterService;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class MailBaseTest {

    protected static WebDriver driver;

    protected AuthenticationService authenticationService;
    protected Letter letter;
    protected LetterService letterService = new LetterService(driver);

    @BeforeTest
    public void setUp() throws MailRuAuthenticationException {
        Browser browser = Browser.getInstance();
        driver = browser.getWrappedDriver();
        authenticationService = new AuthenticationService(driver);
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginUsingUsername(account);
    }

    @AfterTest
    public void tearDown() {
        Browser.getInstance().stopBrowser();
    }
}
