package com.epam.gomel.tat.exception;

public class MailRuAuthenticationException extends Exception {

    public MailRuAuthenticationException(String message) {
        super();
        System.out.println(message);
    }
}
