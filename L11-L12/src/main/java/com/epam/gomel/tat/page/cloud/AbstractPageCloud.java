package com.epam.gomel.tat.page.cloud;

import com.epam.gomel.tat.framework.ui.Element;
import org.openqa.selenium.TimeoutException;

import static org.openqa.selenium.By.xpath;

public abstract class AbstractPageCloud {

    protected static final int PENDING_TIMEOUT = 5;
    protected static final String EXISTING_ELEMENT_LOCATOR_TEMPLATE =
            "//div[@id='datalist']//div[@data-name = 'link'][contains(@data-id, '%s')]";

    protected boolean isElementPresent(String name) {
        Element dynamicElement = new Element(xpath(getElementXpath(name)));
        try {
            dynamicElement.waitForAppear(PENDING_TIMEOUT);
        } catch (TimeoutException e) {
            return false;
        }
        return dynamicElement.isVisible();
    }

    protected String getElementXpath(String elementName) {
        return String.format(EXISTING_ELEMENT_LOCATOR_TEMPLATE, elementName);

    }
}
