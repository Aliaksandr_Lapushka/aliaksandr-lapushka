package com.epam.gomel.tat.page.mail;

public class SentPageMail extends AbstractPageMail {

    private static final String SENT_LETTER_LOCATOR_TEMPLATE =
            "//div[@class='b-datalist b-datalist_letters b-datalist_letters_to']"
                    + "//div[@class='b-datalist__item__subj'][contains(text(),'%s')]";

    public boolean isLetterPresent(String subject) {
        return super.isLetterPresent(SENT_LETTER_LOCATOR_TEMPLATE, subject);
    }
}
