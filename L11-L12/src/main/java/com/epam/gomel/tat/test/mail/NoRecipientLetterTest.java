package com.epam.gomel.tat.test.mail;

import com.epam.gomel.tat.bo.LetterFactory;
import com.epam.gomel.tat.exception.MailRuNoRecipientException;
import org.testng.annotations.Test;

public class NoRecipientLetterTest extends MailBaseTest {

    @Test(description = "Check if alert thrown when attempting to send invalid letter",
            expectedExceptions = MailRuNoRecipientException.class)
    public void sendNoRecipientLetterCheck() throws MailRuNoRecipientException {
        letter = LetterFactory.getNoRecipientLetter();
        letterService.sendLetter(letter);
    }
}
