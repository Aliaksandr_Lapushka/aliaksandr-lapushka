package com.epam.gomel.tat.service.mail;

import com.epam.gomel.tat.bo.Letter;
import com.epam.gomel.tat.exception.MailRuNoRecipientException;
import com.epam.gomel.tat.framework.utils.MailFolder;
import com.epam.gomel.tat.page.mail.*;
import org.openqa.selenium.WebDriver;

public class LetterService {

    private WebDriver driver;
    private LoggedInPageMail loggedInPage;
    private ComposeLetterPage composeLetterPage;
    private SentPageMail sentPage;
    private DraftsPageMail draftsPage;
    private TrashPageMail trashPage;
    private LetterSentPage letterSentPage;

    public LetterService(WebDriver driver) {
        this.driver = driver;
    }

    public void sendLetter(Letter letter) throws MailRuNoRecipientException {
        loggedInPage = new LoggedInPageMail();
        composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.enterRecipient(letter.getRecipient());
        composeLetterPage.enterSubject(letter.getSubject());
        composeLetterPage.enterText(letter.getText());
        letterSentPage = composeLetterPage.sendLetter();
    }

    public void saveAsDraft(Letter letter) {
        loggedInPage = new LoggedInPageMail();
        composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.enterSubject(letter.getSubject());
        composeLetterPage.saveDraft();
        draftsPage = composeLetterPage.toDrafts();
    }

    public void deleteFromDrafts(Letter letter) {
        draftsPage.checkExactDraft(letter.getSubject());
        draftsPage.deleteDraft();
    }

    public void draftPermanentDelete(Letter letter) {
        trashPage = draftsPage.openTrash();
        trashPage.checkExactLetter(letter.getSubject());
        trashPage.permanentDelete();
    }

    public boolean isLetterExists(Letter letter, MailFolder folder) {
        boolean result;
        switch (folder) {

            case INBOX:
                loggedInPage = letterSentPage.redirectToInbox();
                result = loggedInPage.isLetterPresent(letter.getSubject());
                break;

            case OUTBOX:
                sentPage = loggedInPage.toOutbox();
                result = sentPage.isLetterPresent(letter.getSubject());
                break;

            case DRAFTS:
                result = draftsPage.isLetterPresent(letter.getSubject());
                break;

            case TRASH:
                result = trashPage.isLetterPresent(letter.getSubject());
                break;

            default:
                result = false;
        }
        return result;
    }
}
