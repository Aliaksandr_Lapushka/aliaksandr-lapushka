package com.epam.gomel.tat.framework.ui;

public enum BrowserType {
    FIREFOX("firefox"),
    CHROME("chrome"),
    FIREFOX_REMOTE("firefox_remote"),
    CHROME_REMOTE("chrome_remote");

    String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
