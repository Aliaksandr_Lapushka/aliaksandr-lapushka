package com.epam.gomel.tat.page.mail;

import com.epam.gomel.tat.exception.MailRuNoRecipientException;
import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.framework.ui.Element;
import org.openqa.selenium.*;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.xpath;

public class ComposeLetterPage extends AbstractPageMail {

    private static final Element adressInput = new Element(cssSelector("textarea[data-original-name='To']"));
    private static final Element subjectInput = new Element(cssSelector("input[name='Subject']"));
    private static final Element letterBodyFrame = new Element(cssSelector("table.mceLayout iframe"));
    private static final Element letterBody = new Element(cssSelector("#tinymce"));
    private static final Element sendButton = new Element(xpath("(//div[@data-name='send'])[1]"));
    private static final Element confirmPopup =
            new Element(xpath("//div[@class='is-compose-empty_in']//button[@type='submit']"));
    private static final Element saveDraft = new Element(cssSelector("div[data-name = 'saveDraft']"));
    private static final Element toDraftsFolder = new Element(cssSelector("a[href='/messages/drafts/']"));

    public void enterRecipient(String recipient) {
        adressInput.waitForAppear();
        adressInput.clear();
        adressInput.type(recipient);
    }

    public void enterSubject(String subject) {
        subjectInput.waitForAppear();
        subjectInput.clear();
        subjectInput.type(subject);
    }

    public void enterText(String text) {

        letterBodyFrame.waitForAppear();
        letterBodyFrame.switchToFrame();
        letterBody.clear();
        letterBody.type(text);
        letterBodyFrame.switchToDefaultContent();
    }

    public LetterSentPage sendLetter() throws MailRuNoRecipientException {
        sendButton.waitForAppear();
        sendButton.click();
        try {
            Browser.getInstance().waitForAlert();
            String message = Browser.getInstance().fetchAlertText();
            Browser.getInstance().acceptAlert();
            super.openInbox();
            Browser.getInstance().acceptAlert();
            throw new MailRuNoRecipientException(message);
        } catch (TimeoutException e) {
            try {
                confirmPopup.waitForAppear();
                confirmPopup.click();
                return new LetterSentPage();
            } catch (TimeoutException e1) {
                return new LetterSentPage();
            }
        }
    }

    public void saveDraft() {
        saveDraft.waitForAppear();
        saveDraft.click();
    }

    public DraftsPageMail toDrafts() {
        //toDraftsFolder.waitForAppear();
        try {
            toDraftsFolder.waitForAppear(super.ELEMENT_WAIT_TIMEOUT);
            toDraftsFolder.click();
            Browser.getInstance().acceptAlert();
        } catch (NoAlertPresentException e) {
            toDraftsFolder.waitForAppear(super.ELEMENT_WAIT_TIMEOUT);
            toDraftsFolder.click();
        } catch (UnhandledAlertException e) {
            Browser.getInstance().acceptAlert();
            toDraftsFolder.waitForAppear(super.ELEMENT_WAIT_TIMEOUT);
            toDraftsFolder.click();
        }
        return new DraftsPageMail();
    }
}
