package com.epam.gomel.tat.page.mail;

import com.epam.gomel.tat.framework.ui.Element;
import org.openqa.selenium.TimeoutException;

import static org.openqa.selenium.By.xpath;

public abstract class AbstractPageMail {

    protected static final int ELEMENT_WAIT_TIMEOUT = 10;
    private static final Element toInbox =
        new Element(xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/inbox/']"));

    protected boolean isLetterPresent(String template, String subject) {
        String resLocator;
        if ("".equals(subject)) {
            resLocator = String.format(template, "<");
        } else {
            resLocator = String.format(template, subject);
        }

        Element element = new Element(xpath(resLocator));
        try {
            element.waitForAppear(ELEMENT_WAIT_TIMEOUT);
            return element.isVisible();
        } catch (TimeoutException e) {
            return false;
        }
    }

    protected void openInbox() {
        toInbox.waitForAppear();
        toInbox.click();
    }
}
