package com.epam.gomel.tat.page.mail;

import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.framework.ui.Element;
import com.epam.gomel.tat.framework.utils.MailRuData;

import static org.openqa.selenium.By.id;

/**
 * Created by Aliaksandr Lapushka on 24.07.2017.
 */
public class LoginPage {

    private static final Element loginInput = new Element(id("mailbox__login"));
    private static final Element passwordInput = new Element(id("mailbox__password"));
    private static final Element submitButton = new Element(id("mailbox__auth__button"));

    public LoginPage open() {
        Browser.getInstance().navigate(MailRuData.MAIL_HOMEPAGE);
        return this;
    }

    public void enterLogin(String username) {
        loginInput.type(username);
    }

    public void enterPassword(String password) {
        passwordInput.type(password);
    }

    public LoggedInPageMail submit() {
        submitButton.click();
        return new LoggedInPageMail();
    }
}
