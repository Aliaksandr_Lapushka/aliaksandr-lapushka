package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.framework.listener.SuiteListener;
import com.epam.gomel.tat.framework.listener.TestListenerGeneral;
import com.epam.gomel.tat.framework.ui.Browser;
import com.epam.gomel.tat.test.mail.MailBaseTest;
import org.testng.annotations.*;

@Listeners({TestListenerGeneral.class, SuiteListener.class})
public class CloudBaseTest extends MailBaseTest {

    @BeforeTest
    public void setUp() throws MailRuAuthenticationException {
        super.setUp();
        authenticationService.openCloudLoggedinPage();
        for (String winHandle : Browser
                .getInstance()
                .getWrappedDriver()
                .getWindowHandles()) {
            Browser
                    .getInstance()
                    .getWrappedDriver()
                    .switchTo()
                    .window(winHandle);
        }
    }
}
