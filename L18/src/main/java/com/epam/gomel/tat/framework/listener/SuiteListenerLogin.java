package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.reporting.Log;
import org.testng.ISuite;

/**
 * Created by sergey on 28.08.2017.
 */
public class SuiteListenerLogin extends SuiteListener {
    @Override
    public void onStart(ISuite iSuite) {
        Log.info("[SUITE STARTED] " + iSuite.getName());
    }

    @Override
    public void onFinish(ISuite iSuite) {
        super.onFinish(iSuite);
    }
}
