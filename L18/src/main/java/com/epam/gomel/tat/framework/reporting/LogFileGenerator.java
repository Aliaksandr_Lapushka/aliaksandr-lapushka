package com.epam.gomel.tat.framework.reporting;

import org.apache.log4j.FileAppender;
import org.apache.log4j.spi.ErrorCode;

import java.io.File;
import java.io.IOException;

public class LogFileGenerator extends FileAppender {

    public void activateOptions() {
        if (fileName != null) {
            try {
                fileName = getNewLogFileName();
                setFile(fileName, fileAppend, bufferedIO, bufferSize);
            } catch (IOException e) {
                errorHandler.error("Error while activating log options", e,
                        ErrorCode.FILE_OPEN_FAILURE);
            }
        }
    }

    private String getNewLogFileName() {
        if (fileName != null) {
            final String DOT = ".";
            final String HIPHEN = "-";
            final File logFile = new File(fileName);
            final String fileName = logFile.getName();
            String newFileName = "";

            final int dotIndex = fileName.indexOf(DOT);
            if (dotIndex != -1) {
                newFileName = fileName.substring(0, dotIndex)
                        + HIPHEN
                        + System.currentTimeMillis()
                        + DOT
                        + fileName.substring(dotIndex + 1);
            } else {
                newFileName = fileName + HIPHEN + System.currentTimeMillis();
            }
            return logFile.getParent() + File.separator + newFileName;
        }
        return null;
    }
}
