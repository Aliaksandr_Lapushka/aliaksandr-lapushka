package com.epam.gomel.tat.test.mail;

import com.epam.gomel.tat.bo.Account;
import com.epam.gomel.tat.bo.AccountFactory;
import com.epam.gomel.tat.bo.Letter;
import com.epam.gomel.tat.exception.MailRuAuthenticationException;
import com.epam.gomel.tat.framework.listener.SuiteListener;
import com.epam.gomel.tat.framework.listener.TestListenerGeneral;
import com.epam.gomel.tat.service.common.AuthenticationService;
import com.epam.gomel.tat.service.mail.LetterService;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

@Listeners({TestListenerGeneral.class, SuiteListener.class})
public class MailBaseTest {

    //protected static WebDriver driver;

    protected AuthenticationService authenticationService;
    protected Letter letter;
    protected LetterService letterService = new LetterService();

    @BeforeTest
    public void setUp() throws MailRuAuthenticationException {
        authenticationService = new AuthenticationService();
        Account account = AccountFactory.getExistentAccount();
        authenticationService.loginUsingUsername(account);
    }
}
