package com.epam.gomel.tat.framework.ui;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.framework.runner.Parameters;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

// /Lecture 12 material
public final class Browser implements WrapsDriver {

    //private static Browser instance;
    private static final int DEFAULT_TIMEOUT = 5;

    private static ThreadLocal<Browser> instance = new ThreadLocal<Browser>();

    private WebDriver wrappedWebDriver;
    private File screenshotFile;

    private Browser() {
    }

    private static Browser launch() {
        if (instance.get() != null) {
            instance.get().stopBrowser();
        }
        Browser browser = new Browser();
        browser.createDriver();
        instance.set(browser);
        return browser;
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            launch();
        }
        return instance.get();
    }

    private void createDriver() {
        switch (Parameters.instance().getBrowserType()) {
            case FIREFOX:
                this.wrappedWebDriver = firefoxDriver();
                break;
            case CHROME:
                this.wrappedWebDriver = chromeDriver();
                break;
            case FIREFOX_REMOTE:
                this.wrappedWebDriver = firefoxDriverRemote();
                break;
            case CHROME_REMOTE:
                this.wrappedWebDriver = chromeDriverRemote();
                break;
            default:
        }
        configBrowser();
    }

    private WebDriver firefoxDriver() {
        System.setProperty("webdriver.gecko.driver", Parameters.instance().getFirefoxDriver());
        return new FirefoxDriver();
    }

    private String configureSelenium() {
        String config;
        config = String.format("http://%1$s:%2$s/wd/hub", Parameters.instance().getSelenHost(),
                Parameters.instance().getSelenPort());
        return config;
    }

    private WebDriver firefoxDriverRemote() {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        //capabilities.setCapability();
        try {
            return new RemoteWebDriver(new URL(configureSelenium()), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private WebDriver chromeDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("incognito");
        System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
        return new ChromeDriver(options);
    }

    private WebDriver chromeDriverRemote() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("incognito");
        System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setBrowserName("chrome");
        //capabilities.setPlatform(WINDOWS);
        //capabilities.setVersion("60.0.3112.113");
        //capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        try {
            return new RemoteWebDriver(new URL(configureSelenium()), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void configBrowser() {
        wrappedWebDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        wrappedWebDriver.manage().window().maximize();
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void stopBrowser() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } finally {
            instance.set(null);
        }
    }

    public void screenshot() {
        screenshotFile = new File("logs/screenshots/" + System.nanoTime() + ".png");
        try {
            byte[] screenshotBytes = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.info("Saving screenshot: " + screenshotFile.getAbsolutePath());
            Reporter.log(String .format("<a href=%s>Open screenshot</a>", screenshotFile.getAbsolutePath()));
            //return screenshotBytes;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to write screenshot");
        }
        // catch (UnhandledAlertException e) {
        //
        //}
    }

    public void navigate(String url) {
        Log.debug("Opening page: " + url);
        wrappedWebDriver.get(url);
        screenshot();
    }

    public void type(By by, String text) {
        Log.debug(String.format("Send text '%1$s' into web element '%2$s'", text, by.toString()));
        wrappedWebDriver
                .findElement(by)
                .sendKeys(text);
        screenshot();
    }

    public void clear(By by) {
        Log.debug("Clear field: " + by.toString());
        wrappedWebDriver
                .findElement(by)
                .clear();
    }

    public void switchToFrame(By by) {
        Log.debug("Switching to frame: " + by.toString());
        wrappedWebDriver.switchTo()
                .frame(wrappedWebDriver.findElement(by));
    }

    public WebElement findElement(By by) {
        Log.debug("Find web element: " + by.toString());
        return wrappedWebDriver
                .findElement(by);
    }

    public void switchToDefaultContent() {
        Log.debug("Switch to default content");
        wrappedWebDriver.switchTo().defaultContent();
    }

    public String getText(By by) {
        Log.debug("Get text of web element: " + by.toString());
        return wrappedWebDriver
                .findElement(by)
                .getText();
    }

    public void uploadFile(String pathToFile, By fileInput) {
        Log.debug(String.format("Send file '%1$s' into web element '%2$s'", pathToFile, fileInput.toString()));
        wrappedWebDriver
                .findElement(fileInput)
                .sendKeys(pathToFile);
    }

    //My methods
    public void click(By by) {
        Log.debug("Click to: " + by.toString());
        waitForVisibility(by);
        WebElement element = wrappedWebDriver.findElement(by);
        JavascriptExecutor js = (JavascriptExecutor) getWrappedDriver();
        js.executeScript("arguments[0].style.border='4px groove green'", element);
        js.executeScript("arguments[0].style.border=''", element);
        Actions actions = new Actions(wrappedWebDriver);
        actions.moveToElement(element);
        actions.click();
        actions.build().perform();
        //screenshot();
    }

    public boolean isVisible(By by) {
        Log.debug("Web element is visible: " + by.toString());
        return !wrappedWebDriver
                .findElements(by)
                .isEmpty();
    }

    protected void waitForVisibility(By by) {
        Log.debug("Wait for visibility of web element:  " + by.toString());
        new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT)
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    protected void waitForVisibility(By by, int timeout) {
        Log.debug(String.format("Waiting %1$s sec. for visibility of web element %2$s", timeout, by.toString()));
        new WebDriverWait(wrappedWebDriver, timeout)
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForAlert() {
        Log.debug("Wait for alert...");
        new WebDriverWait(wrappedWebDriver, DEFAULT_TIMEOUT)
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent());
        //screenshot();
    }

    public String fetchAlertText() {
        Log.debug("Fetching alert text...");
        return wrappedWebDriver
                .switchTo()
                .alert()
                .getText();
    }

    public void acceptAlert() {
        Log.debug("Accept alert");
        wrappedWebDriver
                .switchTo()
                .alert()
                .accept();
    }
}
