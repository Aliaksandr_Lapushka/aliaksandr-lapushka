package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.page.cloud.LoggedinPageCloud;
import org.apache.commons.lang3.RandomStringUtils;

public class FolderService extends AbstractCloudService {

    private static final int NAME_SIZE = 4;

    //private WebDriver driver;
    private String folderName;

    public String getFolderName() {
        return folderName;
    }

    public void createFolder() {
        folderName = "folder_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);
        Log.info("Creating folder: " + folderName);
        cloudLoggedinPage = new LoggedinPageCloud();
        cloudLoggedinPage.createRemoteFolder(folderName);
    }

    public void openFolder() {
        Log.info("Opening folder: " + folderName);
        cloudLoggedinPage.openFolder(folderName);
    }

    public void goToRoot() {
        Log.info("Opening main page");
        cloudLoggedinPage.goToMain();
    }

    public void deleteFolder() throws Exception {
        Log.info("Deleting folder: " + folderName);
        cloudLoggedinPage.checkExactElement(folderName);
        cloudLoggedinPage.delete();
    }

    public boolean isFolderExists() {
        Log.info("Looking for folder: " + folderName);
        return super.isElementExists(folderName);
    }
}
