package com.epam.gomel.tat.page.mail;

import com.epam.gomel.tat.framework.ui.Element;

import static org.openqa.selenium.By.cssSelector;

public class LetterSentPage {

    private static final Element sentTo = new Element(cssSelector("span.message-sent__info"));
    private static final Element forwardToIncoming =
            new Element(cssSelector("div.message-sent__title a[href='/messages/inbox/']"));

    public LoggedInPageMail redirectToInbox() {
        forwardToIncoming.waitForAppear();
        forwardToIncoming.click();
        return new LoggedInPageMail();
    }
}
