package com.epam.gomel.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.gomel.tat.framework.ui.BrowserType;
import org.testng.xml.XmlSuite;

import java.util.List;

public class Parameters {

    private static Parameters instance;

    @Parameter (names = {"--chrome", "-c"},
            description = "Path to Google Chrome Driver (by default: /chromedriver.exe)")
    private String chromeDriver = "./src/main/resources/driver/chromedriver.exe";
    //private String chromeDriver = "chromedriver.exe";

    @Parameter (names = {"--firefox", "-f"},
            description = "Path to Gecko driver (by default: /geckodriver.exe)")
    private String firefoxDriver = "./src/main/resources/driver/geckodriver.exe";
    //private String firefoxDriver = "geckodriver.exe";

    @Parameter (names = {"--log", "-l"},
            description = "Path to log configuration file (by default: /log4j.properties)")
    private String logger = "./src/main/resources/log4j/log4j.properties";
    //private String logger = "log4j.properties";

    @Parameter(names = {"--browser", "-b"},
            description = "Browser type (chrome or firefox)", converter = BrowserTypeConverter.class)
    //, required = true)
    private BrowserType browserType;
    //= BrowserType.CHROME_REMOTE;

    @Parameter(names = {"--suites", "-s"},
            description = "Suites to run (login or service)")
    private List<String> suites;

    @Parameter(names = {"--parallel", "-pm"},
            description = "Parallel mode setting: false, tests, methods (by default: tests)")
    private XmlSuite.ParallelMode parallelMode = XmlSuite.ParallelMode.TESTS;

    @Parameter(names = {"--threads", "-tc"},
            description = "Number of threads for parallel execution (by default: 2)")
    private int threadCount = 2;

    @Parameter(names = "--help", help = true, description = "Usage instructions")
    private boolean help;

    @Parameter(names = {"--s_host", "-sh"}, description = "Set selenium grid host")
    private String selenHost;

    @Parameter(names = {"--s_port", "-sp"}, description = "Set selenium port")
    private String selenPort;

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public String getFirefoxDriver() {
        return firefoxDriver;
    }

    public String getLogger() {
        return logger;
    }

    public List<String> getSuites() {
        return suites;
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public String getSelenHost() {
        return selenHost;
    }

    public String getSelenPort() {
        return selenPort;
    }

    public XmlSuite.ParallelMode getParallelMode() {
        return parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {

        @Override
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}
