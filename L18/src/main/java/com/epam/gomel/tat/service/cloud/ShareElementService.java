package com.epam.gomel.tat.service.cloud;

import com.epam.gomel.tat.framework.reporting.Log;
import com.epam.gomel.tat.framework.ui.Browser;

import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class ShareElementService extends AbstractCloudService {

    private String elementName;

    public void shareRandomElementName() throws IOException, UnsupportedFlavorException {
        Log.info("Sharing random content...");
        if (cloudLoggedinPage.isEmptyCloud()) {
            //FolderService folderService = new FolderService(driver);
            FolderService folderService = new FolderService();
            folderService.createFolder();
            elementName = folderService.getFolderName();
        } else {
            elementName = cloudLoggedinPage.getRandomElementName();
        }
        Browser
                .getInstance()
                .getWrappedDriver()
                .get(cloudLoggedinPage.getPublicLink(elementName));
    }

    public boolean isCorrectElementWindow() {
        Log.info("Comparing window title vs element name...");
        return Browser
                .getInstance()
                .getWrappedDriver()
                .getTitle()
                .matches(elementName + ".*");
    }

    public void goToRoot() {
        Log.info("Opening main page");
        cloudLoggedinPage.goToMain();
    }
}
