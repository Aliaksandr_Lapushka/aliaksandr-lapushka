package com.epam.gomel.tat.framework.ui;

import org.openqa.selenium.By;

public class Element {

    private By locator;

    public Element(By locator) {
        this.locator = locator;
    }

    public void type(String text) {
        Browser.getInstance().type(locator, text);

    }

    public void click() {
        Browser.getInstance().click(locator);
    }

    public void clear() {
        Browser.getInstance().clear(locator);
    }

    public void waitForAppear() {
        Browser.getInstance().waitForVisibility(locator);
    }

    public void waitForAppear(int timeout) {
        Browser.getInstance().waitForVisibility(locator, timeout);
    }

    public void switchToFrame() {
        Browser.getInstance().waitForVisibility(locator);
        Browser.getInstance().switchToFrame(locator);
    }

    public void switchToDefaultContent() {
        Browser.getInstance().switchToDefaultContent();
    }

    public String getText() {
        return Browser.getInstance().getText(locator);
    }

    public boolean isVisible() {
        return Browser.getInstance().isVisible(locator);
    }

    public void uploadFile(String pathToFile) {
        Browser.getInstance().uploadFile(pathToFile, locator);
    }
}
