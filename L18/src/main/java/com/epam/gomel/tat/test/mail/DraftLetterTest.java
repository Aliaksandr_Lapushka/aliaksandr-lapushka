package com.epam.gomel.tat.test.mail;

import com.epam.gomel.tat.bo.LetterFactory;
import com.epam.gomel.tat.framework.utils.MailFolder;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Aliaksandr Lapushka on 27.07.2017.
 */
public class DraftLetterTest extends MailBaseTest {

    @Test(description = "Check draft saving")
    public void saveDraftCheck() {
        letter = LetterFactory.getDraftLetter();
        letterService.saveAsDraft(letter);
        Assert.assertTrue(letterService.isLetterExists(letter, MailFolder.DRAFTS));
    }

    @Test(description = "Check draft removal from drafts folder", dependsOnMethods = "saveDraftCheck")
    public void deleteFromDraftsCheck() {
        letterService.deleteFromDrafts(letter);
        Assert.assertFalse(letterService.isLetterExists(letter, MailFolder.DRAFTS));
    }

    @Test(description = "Check draft permanent removal", dependsOnMethods = "deleteFromDraftsCheck")
    public void draftDeleteFromTrashCheck() {
        letterService.draftPermanentDelete(letter);
        Assert.assertFalse(letterService.isLetterExists(letter, MailFolder.TRASH));
    }
}
