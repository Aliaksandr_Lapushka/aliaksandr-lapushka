package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.service.cloud.FolderService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class FolderTest extends CloudBaseTest {

    //protected FolderService folderService = new FolderService(driver);
    protected FolderService folderService = new FolderService();

    @Test (description = "Check if created remote folder exists")
    public void remoteFoldercreationCheck() {
        folderService.createFolder();
        Assert.assertTrue(folderService.isFolderExists());
    }

    @Test (description = "Check if folder may be removed", dependsOnMethods = "remoteFoldercreationCheck")
    public void remoteFolderDeleteCheck() throws Exception {
        folderService.deleteFolder();
        Assert.assertFalse(folderService.isFolderExists());

    }

    @AfterClass
    public void prepareGarbage() {
        folderService = null;
    }
}
