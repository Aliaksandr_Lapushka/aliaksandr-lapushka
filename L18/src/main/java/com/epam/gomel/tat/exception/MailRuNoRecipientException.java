package com.epam.gomel.tat.exception;

public class MailRuNoRecipientException extends Exception {

    public MailRuNoRecipientException(String message) {
        super(message);
    }
}
