package com.epam.gomel.tat.framework.appender;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

public class TestNGReportAppender extends AppenderSkeleton {

    @Override
    public boolean requiresLayout() {
        return true;
    }

    @Override
    protected void append(LoggingEvent loggingEvent) {
        Reporter.log(layout.format(loggingEvent));
        //String path = String .format("<a href=%s>screenshot.file</a>", filePath);
        //Reporter.log(path);
    }

    @Override
    public void close() {

    }
}
