package com.epam.gomel.tat.bo;

import com.epam.gomel.tat.framework.utils.MailRuData;

public class AccountFactory {

    public static Account getExistentAccount() {
        return new Account(MailRuData.VALID_TEST_LOGIN, "", MailRuData.VALID_TEST_PASSWORD);
    }

    public static Account getInvalidAccount() {
        Account account = new Account();
        account.setEmail(MailRuData.INVALID_TEST_LOGIN);
        account.setUsername("");
        account.setPassword(MailRuData.VALID_TEST_PASSWORD);
        return account;
    }
}
