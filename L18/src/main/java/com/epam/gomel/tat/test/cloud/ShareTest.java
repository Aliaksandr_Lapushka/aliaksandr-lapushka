package com.epam.gomel.tat.test.cloud;

import com.epam.gomel.tat.service.cloud.ShareElementService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class ShareTest extends CloudBaseTest {

    //private ShareElementService shareElementService = new ShareElementService(driver);
    private ShareElementService shareElementService = new ShareElementService();

    @Test(description = "Share cloud content test")
    public void contentShareCheck() throws IOException, UnsupportedFlavorException {
        shareElementService.shareRandomElementName();
        Assert.assertTrue(shareElementService.isCorrectElementWindow());
        shareElementService.goToRoot();
    }
}
