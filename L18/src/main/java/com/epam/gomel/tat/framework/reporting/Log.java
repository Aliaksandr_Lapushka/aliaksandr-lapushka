package com.epam.gomel.tat.framework.reporting;

import com.epam.gomel.tat.framework.runner.Parameters;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;

public class Log {

    static {
        File log4jfile = new File(Parameters.instance().getLogger());
        PropertyConfigurator.configure(log4jfile.getAbsolutePath());
    }

    private static final Logger LOG = Logger.getLogger("com.epam.gomel.tat");

    public static void info(String message) {
        LOG.info(message);
    }

    public static void error(String message, Throwable e) {
        LOG.error(message, e);
    }

    public static void debug(String message) {
        LOG.debug(message);
    }
}
