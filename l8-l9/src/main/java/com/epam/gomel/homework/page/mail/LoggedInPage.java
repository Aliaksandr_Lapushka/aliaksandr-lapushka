package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Aliaksandr Lapushka on 24.07.2017.
 */
public class LoggedInPage extends PageObject {

    protected static final String ACCOUNT_NAME_LOCATOR = "#PH_user-email";
    protected static final String WRITE_NEW_LETTER_LOCATOR = "a[data-name='compose']";
    protected static final String INBOX_FOLDER_LOCATOR = "div[style=''] a[href='/messages/inbox/']";
    protected static final String SENT_FOLDER_LOCATOR = "a[href='/messages/sent/']";
    protected static final String DELETE_BUTTON_LOCATOR = "(//div[@class='b-toolbar__item'])[5]";
    protected static final String LETTER_LOCATOR_TEMPLATE = "//a[@data-subject='%s']";
    protected static final String EMPTY_LETTER_LOCATOR =
            "//a[@data-subject]//div[@class='b-datalist__item__subj'][contains(text(),'<')]";

    @FindBy(how = How.CSS, using = ACCOUNT_NAME_LOCATOR)
    public WebElement mailbox;

    @FindBy(how = How.CSS, using = INBOX_FOLDER_LOCATOR)
    private WebElement inbox;

    @FindBy(how = How.CSS, using = SENT_FOLDER_LOCATOR)
    private WebElement outbox;

    @FindBy(how = How.CSS, using = WRITE_NEW_LETTER_LOCATOR)
    private WebElement writeNewLetterButton;

    @FindBy(how = How.XPATH, using = DELETE_BUTTON_LOCATOR)
    private WebElement deleteButton;

    public LoggedInPage(WebDriver driver) {
        super(driver);
    }

    public boolean isLetterExists(String subject) {
        String resultingLocator = String.format(LETTER_LOCATOR_TEMPLATE, subject);
        WebElement element;
        //System.out.println("DraftsPage.isLetterExists.resultingLocator " + resultingLocator);
        waitForLoad(driver);
        //waitForVisibility(driver.findElement(By.xpath(resultingLocator)));
        waitForReadiness(By.xpath(resultingLocator));
        //boolean result = !driver.findElements(By.xpath(resultingLocator)).isEmpty();
        boolean result = driver
                .findElement(By.xpath(resultingLocator))
                .isDisplayed();
        return result;
    }

    public boolean isEmptySubjExists() {
        boolean exists = false;
        By by = By.xpath(EMPTY_LETTER_LOCATOR);
        waitForReadiness(by);
        boolean result = !driver.findElements(by).isEmpty();
        return result;
    }

    public ComposeLetterPage composeNewLetter() {
        waitForVisibility(writeNewLetterButton);
        writeNewLetterButton.click();
        return new ComposeLetterPage(driver);
    }

    public String getAccountName() {
        waitForVisibility(mailbox);
        return mailbox.getText();
    }

    public SentPage openSentFolder() {
        waitForVisibility(outbox);
        outbox.click();
        waitForLoad(driver);
        return new SentPage(driver);
    }
}
