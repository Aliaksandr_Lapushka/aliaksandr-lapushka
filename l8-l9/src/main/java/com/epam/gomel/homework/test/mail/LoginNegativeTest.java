package com.epam.gomel.homework.test.mail;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginNegativeTest extends Preconditions {

    @Test(description = "Checks invalid login scenario")
    public void inValidLoginCheck() {
        Assert.assertTrue(signUpPage.loginAttemptFailed());
    }
}
