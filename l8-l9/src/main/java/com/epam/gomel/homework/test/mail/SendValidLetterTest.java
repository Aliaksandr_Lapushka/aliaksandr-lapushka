package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.bo.LetterFactory;
import com.epam.gomel.homework.page.mail.ComposeLetterPage;
import com.epam.gomel.homework.page.mail.LetterSentPage;
import com.epam.gomel.homework.page.mail.SentPage;
import com.epam.gomel.homework.framework.utils.MailRuData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendValidLetterTest extends LoggedInPreconditions {

    private String subject = LetterFactory.createLeterSubject();
    private String text = LetterFactory.createLetterText();

    @Test(description = "Checks valid letter forwarding", priority = 0)
    public void sendValidLetterCheck() {
        ComposeLetterPage composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.fillAllFields(subject, text);
        LetterSentPage letterSentPage = composeLetterPage.sendLetter();
        Assert.assertEquals(letterSentPage.getSentTo(), MailRuData.VALID_TEST_LOGIN);
        letterSentPage.redirectToInbox();
    }

    @Test(description = "Checks letter is present in inbox folder", dependsOnMethods = "sendValidLetterCheck")
    public void isInInboxCheck() {
        Assert.assertTrue(loggedInPage.isLetterExists(subject));
    }

    @Test(description = "Checks letter is present in outbox folder", dependsOnMethods = "isInInboxCheck")
    public void isInOutboxCheck() {
        SentPage sentPage = loggedInPage.openSentFolder();
        Assert.assertTrue(sentPage.isLetterExists(subject));
    }
}
