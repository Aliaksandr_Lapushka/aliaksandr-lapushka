package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TrashPage extends PageObject {

    protected static final String DELETED_LETTER_LOCATOR_TEMPLATE = "//a[@data-subject='%s']";
    protected static final String LETTER_CHECKBOX_LOCATOR_TEMPLATE = "//a[@data-subject='%s']"
            + "//div[@class='b-checkbox__box']";
    protected static final String DELETE_BUTTON_LOCATOR = "(//div[@style='']//div[@data-name='remove'])[1]";

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public boolean isLetterExists(String subject) {
        String resultingLocator = String.format(DELETED_LETTER_LOCATOR_TEMPLATE, subject);
        //System.out.println("DraftsPage.isLetterExists.resultingLocator " + resultingLocator);
        waitForReadines(driver.findElement(By.xpath(resultingLocator)));
        boolean result = !driver.findElements(By.xpath(resultingLocator)).isEmpty();
        return result;
    }

    public boolean isNoLetterExists(String subject) {
        String resultingLocator = String.format(DELETED_LETTER_LOCATOR_TEMPLATE, subject);
        //System.out.println("DraftsPage.isLetterExists.resultingLocator " + resultingLocator);
        waitForLoad(driver);
        //waitForReadines(driver.findElement(By.xpath(resultingLocator)));
        boolean result = driver.findElements(By.xpath(resultingLocator)).isEmpty();
        return result;
    }

    public void checkExactLetter(String subject) {
        if (isLetterExists(subject)) {
            String resultingLocator = String.format(LETTER_CHECKBOX_LOCATOR_TEMPLATE, subject);
            waitForReadines(driver.findElement(By.xpath(resultingLocator)));
            System.out.println("DraftsPage.checkExactDraft.resultingLocator " + resultingLocator);
            WebElement checkbox = driver.findElement(By.xpath(resultingLocator));
            waitForClickable(By.xpath(resultingLocator));
            checkbox.click();
        }
    }

    public void delete() {
        WebElement element = driver.findElement(By.xpath(DELETE_BUTTON_LOCATOR));
        waitForReadines(element);
        element.click();
    }
}
