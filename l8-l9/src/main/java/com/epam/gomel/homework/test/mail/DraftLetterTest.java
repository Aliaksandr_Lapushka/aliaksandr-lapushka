package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.bo.LetterFactory;
import com.epam.gomel.homework.page.mail.ComposeLetterPage;
import com.epam.gomel.homework.page.mail.DraftsPage;
import com.epam.gomel.homework.page.mail.TrashPage;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by sergey on 27.07.2017.
 */
public class DraftLetterTest extends LoggedInPreconditions {

    private String subject = LetterFactory.createDraftSubject();
    private DraftsPage draftsPage;
    private TrashPage trashPage;

    @Test (description = "Check if draft been created", priority = 0)
    public void saveDraftCheck() {
        ComposeLetterPage composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.fillSubjectOnly(subject);
        composeLetterPage.saveDraft();
        draftsPage = composeLetterPage.openDrafts();
        Assert.assertTrue(draftsPage.isLetterExists(subject));
    }

    @Test (description = "Check if deleted draft is in trash", priority = 1)
    public void deleteDraftCheck() {
        //loggedInPage.openDrafts();
        draftsPage.checkExactDraft(subject);
        draftsPage.deleteDraft();
        trashPage = draftsPage.openTrash();
        Assert.assertTrue(draftsPage.isLetterExists(subject));
    }

    @Test (description = "Check if draft been permanently deleted", priority = 2)
    public void permanentDeleteDraftCheck() {
        trashPage.checkExactLetter(subject);
        trashPage.delete();
        Assert.assertFalse(trashPage.isNoLetterExists(subject));
    }
}
