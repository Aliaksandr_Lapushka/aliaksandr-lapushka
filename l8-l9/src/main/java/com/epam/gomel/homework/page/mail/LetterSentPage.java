package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LetterSentPage extends PageObject {

    protected static final String SENT_TO = "span.message-sent__info";
    protected static final String FORWARD_TO_INCOMING = "div.message-sent__title a[href='/messages/inbox/']";

    @FindBy(how = How.CSS, using = SENT_TO)
    private WebElement destination;

    @FindBy(how = How.CSS, using = FORWARD_TO_INCOMING)
    private WebElement forwardToIncoming;

    public LetterSentPage(WebDriver driver) {
        super(driver);
    }

    public String getSentTo() {
        waitForVisibility(destination);
        return destination.getText();
    }

    public LoggedInPage redirectToInbox() {
        waitForVisibility(forwardToIncoming);
        forwardToIncoming.click();
        waitForLoad(driver);
        //new WebDriverWait(driver, 10);
        return new LoggedInPage(driver);
    }
}
