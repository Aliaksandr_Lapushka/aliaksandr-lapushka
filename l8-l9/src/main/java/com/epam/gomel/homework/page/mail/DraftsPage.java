package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.util.List;

public class DraftsPage extends SentPage {

    protected static final String CHECKBOX_LOCATOR = "a[data-subject] div[class='b-checkbox__box']";
    protected static final String DRAFT_LETTER_LOCATOR_TEMPLATE = "//a[@data-subject='%s']";
    protected static final String LETTER_CHECKBOX_LOCATOR_TEMPLATE = "//a[@data-subject='%s']"
            + "//div[@class='b-checkbox__box']";
    protected static final String DELETE_BUTTON_LOCATOR = "(//div[@style='']//div[@data-name='remove'])[1]";
    protected static final String TRASH_FOLDER_LOCATOR = "//i[@class='ico ico_folder ico ico_folder_trash']";

    @FindBy(how = How.CSS, using = CHECKBOX_LOCATOR)
    private WebElement draftCheckbox;

    @FindBy(how = How.XPATH, using = TRASH_FOLDER_LOCATOR)
    private WebElement trashFolder;

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> collectElements(String locator) {
        try {
            waitForVisibility(driver.findElement(By.xpath(locator)));
        } catch (NoSuchElementException e) {
            System.out.println("class DraftsPage, Folder is empty");
        }
        By by = By.xpath(locator);
        List<WebElement> elementList = driver.findElements(by);
        return elementList;
    }

    public boolean isLetterExists(String subject) {
        String resultingLocator = String.format(DRAFT_LETTER_LOCATOR_TEMPLATE, subject);
        By by = By.xpath(resultingLocator);
        //System.out.println("DraftsPage.isLetterExists.resultingLocator " + resultingLocator);
        //waitForLoad(driver);
        waitForReadiness(by);
        //waitForVisibility(driver.findElement(By.xpath(resultingLocator)));
        boolean result = !driver.findElements(by).isEmpty();
        return result;
    }

    public void checkExactDraft(String subject) {
        if (isLetterExists(subject)) {
            String resultingLocator = String.format(LETTER_CHECKBOX_LOCATOR_TEMPLATE, subject);
            System.out.println("DraftsPage.checkExactDraft.resultingLocator " + resultingLocator);
            WebElement checkbox = driver.findElement(By.xpath(resultingLocator));
            waitForClickable(By.xpath(resultingLocator));
            checkbox.click();
        }
    }

    public void deleteDraft() {
        WebElement element = driver.findElement(By.xpath(DELETE_BUTTON_LOCATOR));
        waitForReadines(element);
        element.click();
    }

    public TrashPage openTrash() {
        waitForReadines(trashFolder);
        trashFolder.click();
        waitForLoad(driver);
        return new TrashPage(driver);
    }
}
