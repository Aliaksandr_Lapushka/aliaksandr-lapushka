package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.page.mail.ComposeLetterPage;
import com.epam.gomel.homework.page.mail.LetterSentPage;
import com.epam.gomel.homework.page.mail.SentPage;
import com.epam.gomel.homework.framework.utils.MailRuData;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Aliaksandr Lapushka on 27.07.2017.
 */
public class EmptySubjLetterTest extends LoggedInPreconditions {
    @Test(description = "Checks empty subject and body letter forwarding", priority = 0)
    public void sendEmptyLetterCheck() {
        ComposeLetterPage composeLetterPage = loggedInPage.composeNewLetter();
        composeLetterPage.fillRecipientOnly();
        LetterSentPage letterSentPage = composeLetterPage.sendEmptyLetter();
        Assert.assertEquals(letterSentPage.getSentTo(), MailRuData.VALID_TEST_LOGIN);
        letterSentPage.redirectToInbox();
    }

    @Test(description = "Checks empty letter is present in inbox folder", priority = 1)
    public void isInInboxCheck() {
        Assert.assertTrue(loggedInPage.isEmptySubjExists());
    }

    @Test(description = "Checks letter is present in outbox folder", priority = 2)
    public void isInOutboxCheck() {
        SentPage sentPage = loggedInPage.openSentFolder();
        Assert.assertTrue(sentPage.isEmptySubjExists());
    }

}
