package com.epam.gomel.homework.page.mail;

import com.epam.gomel.homework.framework.utils.MailRuData;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ComposeLetterPage extends PageObject {

    protected static final String ADDRESS_INPUT_LOCATOR = "textarea[data-original-name='To']";
    protected static final String SUBJ_INPUT_LOCATOR = "input[name='Subject']";
    protected static final String LETTER_BODY_FRAME = "table.mceLayout iframe";
    protected static final String LETTER_BODY = "#tinymce";
    protected static final String SEND_LETTER_BUTTON_LOCATOR = "(//div[@data-name='send'])[1]";
    //protected static final String TEST_SUBJ = "Subject";
    //protected static final String DRAFT_SUBJ = "Draft Subject";
    protected static final String CONFIRM_POPUP_LOCATOR = "//div[@class='is-compose-empty_in']//button[@type='submit']";
    protected static final String SAVE_DRAFT_BUTTON_LOCATOR = "div[data-name = 'saveDraft']";
    protected static final By DRAFT_FOLDER_LOCATOR = By.cssSelector("a[href='/messages/drafts/']");

    private boolean isAlertPresent;

    @FindBy(how = How.CSS, using = ADDRESS_INPUT_LOCATOR)
    private WebElement recepient;

    @FindBy(how = How.CSS, using = SUBJ_INPUT_LOCATOR)
    private WebElement subjectInput;

    @FindBy(how = How.CSS, using = LETTER_BODY_FRAME)
    private WebElement letterBodyFrame;

    @FindBy(how = How.XPATH, using = SEND_LETTER_BUTTON_LOCATOR)
    private WebElement sendLetter;

    @FindBy(how = How.XPATH, using = CONFIRM_POPUP_LOCATOR)
    private WebElement confirmPopup;

    @FindBy(how = How.CSS, using = SAVE_DRAFT_BUTTON_LOCATOR)
    private WebElement saveDraftButton;

    public ComposeLetterPage(WebDriver driver) {
        super(driver);
    }

    public void fillAllFields(String subj, String text) {
        waitForVisibility(letterBodyFrame);
        recepient.clear();
        recepient.sendKeys(MailRuData.VALID_TEST_LOGIN);
        subjectInput.clear();
        subjectInput.sendKeys(subj);
        driver.switchTo().frame(letterBodyFrame);
        driver.findElement(By.cssSelector(LETTER_BODY))
                .sendKeys(text);
        driver.switchTo().defaultContent();
    }

    public void fillRecipientOnly() {
        waitForVisibility(letterBodyFrame);
        recepient.clear();
        recepient.sendKeys(MailRuData.VALID_TEST_LOGIN);
    }

    public void fillSubjectOnly(String subject) {
        waitForVisibility(letterBodyFrame);
        subjectInput.clear();
        subjectInput.sendKeys(subject);
    }

    public LetterSentPage sendLetter() {
        waitForVisibility(recepient);
        sendLetter.click();
        return new LetterSentPage(driver);
    }

    public LetterSentPage sendEmptyLetter() {
        waitForVisibility(sendLetter);
        sendLetter.click();
        waitForVisibility(confirmPopup);
        confirmPopup.click();
        return new LetterSentPage(driver);
    }

    public boolean isInvalidLetter() throws InterruptedException {
        try {
            sendLetter();
            waitForAlert(driver.switchTo().alert());
            driver.switchTo().alert().dismiss();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    public void saveDraft() {
        waitForVisibility(saveDraftButton);
        saveDraftButton.click();
        try {
            waitForAlert(driver.switchTo().alert());
            driver.switchTo()
                    .alert()
                    .accept();
        } catch (NoAlertPresentException e) {
            saveDraftButton.click();
        } catch (UnhandledAlertException e) {
            driver.switchTo()
                    .alert()
                    .accept();
            saveDraftButton.click();
        }
        waitForLoad(driver);
    }

    public DraftsPage openDrafts() {
        waitForReadiness(DRAFT_FOLDER_LOCATOR);
        try {
            //clickOn(DRAFT_FOLDER_LOCATOR, driver);
            driver.findElement(DRAFT_FOLDER_LOCATOR).click();
            waitForAlert(driver.switchTo().alert());
            driver.switchTo()
                    .alert()
                    .accept();
        } catch (NoAlertPresentException e) {
            //clickOn(DRAFT_FOLDER_LOCATOR, driver);
            waitForReadiness(DRAFT_FOLDER_LOCATOR);
            driver.findElement(DRAFT_FOLDER_LOCATOR).click();
        } catch (UnhandledAlertException e) {
            waitForReadiness(DRAFT_FOLDER_LOCATOR);
            driver.switchTo()
                    .alert()
                    .accept();
            //clickOn(DRAFT_FOLDER_LOCATOR, driver);
            driver.findElement(DRAFT_FOLDER_LOCATOR).click();
        }
        return new DraftsPage(driver);
    }
}
