package com.epam.gomel.homework.bo;

import org.apache.commons.lang3.RandomStringUtils;

public class LetterFactory {

    private static final int NAME_SIZE = 4;
    private static final int MESSAGE_LINE_SIZE = 4;

    public static String createLeterSubject() {
        String subjName = "Subject_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);

        return subjName;
    }

    public static String createDraftSubject() {
        String subjName = "Draft_" + RandomStringUtils.randomAlphabetic(NAME_SIZE);

        return subjName;
    }

    public static String createLetterText() {
        String text = "Line " + RandomStringUtils.randomAlphabetic(MESSAGE_LINE_SIZE);
        return text;
    }

}
