package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.page.mail.ComposeLetterPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Aliaksandr Lapushka on 27.07.2017.
 */
public class InvalidRecepientTest extends LoggedInPreconditions {

    @Test(description = "Checks if error is present when recipient is wrong")
    public void invalidLetterCheck() throws InterruptedException {
        ComposeLetterPage composeLetterPage = loggedInPage.composeNewLetter();
        Assert.assertTrue(composeLetterPage.isInvalidLetter());
    }
}
