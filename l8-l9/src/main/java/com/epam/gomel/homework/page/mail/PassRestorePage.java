package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PassRestorePage extends PageObject {

    public static final By ADVANCED_ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='b-login__errors']");

    public PassRestorePage(WebDriver driver) {
        super(driver);
    }

    public boolean wrongCredentials() {
        waitForReadiness(ADVANCED_ERROR_MESSAGE_LOCATOR);
        return driver.findElement(ADVANCED_ERROR_MESSAGE_LOCATOR).isDisplayed();
    }
}
