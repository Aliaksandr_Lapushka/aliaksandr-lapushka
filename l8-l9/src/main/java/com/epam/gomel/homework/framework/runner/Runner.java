package com.epam.gomel.homework.framework.runner;

import org.testng.TestNG;
import java.util.Arrays;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();

        List<String> files = Arrays.asList(
                "./src/main/resources/suite/allTestSuite.xml"
        );

        testNG.setTestSuites(files);
        testNG.run();
    }
}
