package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.page.mail.LoggedInPage;
import com.epam.gomel.homework.framework.utils.MailRuData;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Aliaksandr Lapushka on 23.07.2017.
 */
public class LoginPositiveTest extends Preconditions {

    LoggedInPage loggedInPage;

    @Test (description = "Checks login with valid credentials", groups = "login pos")
    public void validLoginCheck() {

        signUpPage.enterLogin();
        signUpPage.enterPassword();

        loggedInPage = signUpPage.submit();

        Assert.assertEquals(loggedInPage.getAccountName(), MailRuData.VALID_TEST_LOGIN);
    }
}
