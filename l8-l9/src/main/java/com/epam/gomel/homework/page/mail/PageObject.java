package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by Aliaksandr Lapushka on 24.07.2017.
 */
public class PageObject {

    private static final int DEF_TIMEOUT = 30;
    private static final int POLLING = 500;

    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickOn(By locator, WebDriver driver) {
        final WebDriverWait wait = new WebDriverWait(driver, DEF_TIMEOUT);
        wait.until(ExpectedConditions.refreshed(
                ExpectedConditions.elementToBeClickable(locator)));
        driver.findElement(locator).click();
    }

    public void waitForVisibility(WebElement element) {
        new WebDriverWait(driver, DEF_TIMEOUT)
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForReadines(WebElement element) {
        FluentWait fWait = new FluentWait(driver)
                .withTimeout(DEF_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(POLLING, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);
        fWait.until(ExpectedConditions.visibilityOf(element));
        fWait.until(ExpectedConditions.elementToBeClickable(element));

    }

    public void waitForReadiness(By locator) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(DEF_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(POLLING, TimeUnit.MILLISECONDS)
                .ignoring(org.openqa.selenium.NoSuchElementException.class);
        WebElement webElement = wait.until(
                new Function<WebDriver, WebElement>() {
                    public WebElement apply(WebDriver driver) {
                        return driver.findElement(locator);
                    }
                }
        );
        //return webElement;
    }

    public void waitForClickable(By by) {
        new WebDriverWait(driver, DEF_TIMEOUT)
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    public void waitForAlert(Alert alert) {
        new WebDriverWait(driver, DEF_TIMEOUT)
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent());
    }

    public void waitForLoad(WebDriver driver) {
        new WebDriverWait(driver, DEF_TIMEOUT).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }
}
