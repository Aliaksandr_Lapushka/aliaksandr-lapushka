package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.page.mail.LoggedInPage;
import com.epam.gomel.homework.page.mail.SignUpPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoggedInPreconditions {

    public static WebDriver driver;
    public static SignUpPage signUpPage;

    protected static final int DEF_TIMEOUT = 0;

    //private static ChromeOptions options;

    public LoggedInPage loggedInPage;

    static {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeClass(description = "Chrome browser launch")
    protected void setUp() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("incognito");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(DEF_TIMEOUT, TimeUnit.SECONDS);
        signUpPage = new SignUpPage(driver);
        signUpPage.open();
        signUpPage.enterLogin();
        signUpPage.enterPassword();
        loggedInPage = signUpPage.submit();
    }

    @AfterClass(description = "Cookies clean up")
    protected void cleanUp() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass(description = "Close driver")
    protected void tearDown() {
        driver.close();
    }
}
