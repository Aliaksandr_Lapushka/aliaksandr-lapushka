package com.epam.gomel.homework.page.mail;

import com.epam.gomel.homework.framework.utils.MailRuData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Aliaksandr Lapushka on 24.07.2017.
 */
public class SignUpPage extends PageObject {

    //public static final By ERROR_MESSAGE_LOCATOR = By.id("mailbox:authfail");
    //public static final By ADVANCED_ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='b-login__errors']");
    protected static final String ACCOUNT_NAME_LOCATOR = "#PH_user-email";

    @FindBy(id = "mailbox__login")
    private WebElement login;

    @FindBy(id = "mailbox__password")
    private WebElement password;

    @FindBy(id = "mailbox__auth__button")
    private WebElement authButton;

    @FindBy(id = "mailbox:authfail")
    private WebElement errorMessage;

    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(MailRuData.MAIL_HOMEPAGE);
    }

    public void enterLogin() {
        this.login.clear();
        this.login.sendKeys(MailRuData.VALID_TEST_LOGIN);
    }

    public void enterPassword() {
        this.password.clear();
        this.password.sendKeys(MailRuData.VALID_TEST_PASSWORD);
    }

    public LoggedInPage submit() {
        authButton.click();
        return new LoggedInPage(driver);
    }

    public boolean loginAttemptFailed() {
        boolean result = false;
        this.login.clear();
        this.login.sendKeys(MailRuData.INVALID_TEST_LOGIN);
        this.password.clear();
        this.password.sendKeys(MailRuData.VALID_TEST_PASSWORD);
        authButton.click();
        waitForLoad(driver);
        if (!driver
                .findElement(By.cssSelector(ACCOUNT_NAME_LOCATOR))
                .isDisplayed()) {
            result = true;
            //return driver.findElement(ERROR_MESSAGE_LOCATOR).isDisplayed();
        }
        return result;
    }
}
