package com.epam.gomel.homework.page.mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends LoggedInPage {

    public static final String SENT_LETTER_LOCATOR_TEMPLATE =
            "//div[@class='b-datalist b-datalist_letters b-datalist_letters_to']"
                    + "//div[@class='b-datalist__item__subj'][contains(text(),'%s')]";
    protected static final String SENT_EMPTY_LETTER_LOCATOR =
            "//div[@class='b-datalist b-datalist_letters b-datalist_letters_to']"
                    + "//div[@class='b-datalist__item__subj'][contains(text(),'<')]";

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public boolean isLetterExists(String subject) {
        String resultingLocator = String.format(SENT_LETTER_LOCATOR_TEMPLATE, subject);
        By by = By.xpath(resultingLocator);
        System.out.println("DraftsPage.isLetterExists.resultingLocator " + resultingLocator);
        waitForReadiness(by);
        boolean result = !driver.findElements(by).isEmpty();
        return result;
    }

    public boolean isEmptySubjExists() {
        boolean exists = false;
        By by = By.xpath(SENT_EMPTY_LETTER_LOCATOR);
        waitForReadiness(by);
        boolean result = !driver.findElements(by).isEmpty();
        return result;
    }
}
