package com.epam.gomel.homework.test.mail;

import com.epam.gomel.homework.page.mail.SignUpPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

/**
 * Created by Aliaksandr Lapushka on 23.07.2017.
 */
public class Preconditions {

    public static WebDriver driver;
    public static SignUpPage signUpPage;

    protected static final int DEF_TIMEOUT = 0;

    static {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeClass(description = "Chrome browser launch")
    protected void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(DEF_TIMEOUT, TimeUnit.SECONDS);
        signUpPage = new SignUpPage(driver);
        signUpPage.open();
    }

    @AfterClass(description = "Cookies clean up")
    protected void cleanUp() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass(description = "Close driver")
    protected void tearDown() {
        driver.close();
    }
}
