package com.epam.tat;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Class containing static method that accepts given string and creates
 * defined shape instance.
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public abstract class AbstractShapeFinder {

    protected static Shape getShape(String string) throws IllegalArgumentException {

        Shape shape = null;

        ArrayList<String> tokens = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(string, ":");
        while (st.hasMoreTokens()) {
            tokens.add(st.nextToken());
        }
        tokens.replaceAll(String::toUpperCase);

        switch (Shapes.valueOf(tokens.get(0))) {
            case RECTANGLE:
                shape = new Rectangle(Integer.parseInt(tokens.get(1)), Integer.parseInt(tokens.get(2)));
                break;

            case CIRCLE:
                shape = new Circle(Integer.parseInt(tokens.get(1)));
                break;

            case SQUARE:
                shape = new Square(Integer.parseInt(tokens.get(1)));
                break;

            case TRIANGLE:
                shape = new Triangle(Integer.parseInt(tokens.get(1)));
                break;

            default:
                throw new IllegalArgumentException("Invalid input");
        }

        return shape;
    }
}
