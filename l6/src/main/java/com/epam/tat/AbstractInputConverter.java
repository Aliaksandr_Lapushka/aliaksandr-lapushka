package com.epam.tat;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Checks whether conditions are passed via console or from file and creates
 * a collection which stores conditions
 *
 * @version 10.07.2017
 *
 * @author Aliaksandr Lapushka
 */
public class AbstractInputConverter {

    protected static Set<String> inputProcessor(String[] string) {
        Set initValues = null;

        switch (string[0]) {
            case "--plain":
                initValues = new HashSet<String>();
                for (int i = 1; i < string.length; i++) {
                    initValues.add(string[i]);
                }
                break;

            case "--path":
                try {
                    Scanner s = new Scanner(new File(string[1]));
                    initValues = new HashSet<String>();
                    while (s.hasNextLine()) {
                        initValues.add(s.nextLine());
                    }
                    s.close();
                } catch (FileNotFoundException e) {
                    System.out.println("File not found");
                }
                break;

            default:
                initValues = null;
        }
        return initValues;
    }
}
