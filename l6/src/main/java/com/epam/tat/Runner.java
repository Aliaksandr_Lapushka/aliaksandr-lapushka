package com.epam.tat;

import java.util.*;

/**
 * Main class containing entry point.
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public class Runner {

    public static void main(String[] args) {

        String className;
        List<Shape> sortedContainer;
        Set<Shape> shapesContainer = new HashSet<Shape>();

        try {
            for (String obj : AbstractInputConverter.inputProcessor(args)) {
                shapesContainer.add(AbstractShapeFinder.getShape(obj));
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid input");
        }

        sortedContainer = new ArrayList<Shape>(shapesContainer);

        Collections.sort(sortedContainer);

        for (Shape j : sortedContainer) {
            className = j.getClass().getSimpleName();
            System.out.println(className + " - " + ((Shape) j).getPar());
            System.out.println("    Area: " + ((Shape) j).area() + "\n" + "    Perimeter: "
                    + ((Shape) j).perimeter() + "\n");
        }
    }
}
