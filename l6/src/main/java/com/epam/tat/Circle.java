package com.epam.tat;

/**
 * This class provides shape specific methods to calculate area and perimeter
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public class Circle implements Shape {

    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public String getPar() {
        String par = Integer.toString(radius);

        return par;
    }

    public int perimeter() {
        return (int) (2 * radius * Math.PI);
    }

    public int area() {
        return (int) (radius * radius * Math.PI);
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Circle)) {
            return false;
        }

        Circle other = (Circle) o;
        return this.radius == other.radius;
    }

    public int hashCode() {
        return (int) radius;
    }

    @Override
    public int compareTo(Shape o) {
        int compareArea = o.area();

        return compareArea - this.area();
    }

}
