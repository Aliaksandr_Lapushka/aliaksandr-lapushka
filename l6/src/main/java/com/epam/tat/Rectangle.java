package com.epam.tat;

/**
 * This class provides shape specific methods to calculate area and perimeter
 *
 * @version     06.07.2017
 * @author      Aliaksandr Lapushka
 */
public class Rectangle implements Shape {

    private int side1;
    private int side2;

    public Rectangle() {
    }

    public Rectangle(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    public String getPar() {
        String par1 = Integer.toString(side1);
        String par2 = Integer.toString(side2);

        return par1 + " " + par2;
    }

    public int perimeter() {
        return (side1 + side2) * 2;
    }

    public int area() {
        return side1 * side2;
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Rectangle)) {
            return false;
        }

        Rectangle other = (Rectangle) o;
        return (this.side1 == other.side1) || (this.side2 == other.side2);
    }

    public int hashCode() {
        return (int) side1 * side2;
    }

    @Override
    public int compareTo(Shape o) {
        int compareArea = o.area();

        return compareArea - this.area();
    }

}
